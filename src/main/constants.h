#ifndef __CONSTANTS_H
#define __CONSTANTS_H

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

#define PORT 8008

#define SIZE 1024

#define WIDTH_WINDOW 1920
#define HEIGHT_WINDOW 1080

#define WIDTH_MENU 400
#define HEIGHT_MENU HEIGHT_WINDOW

#define WIDTH_AREA (WIDTH_WINDOW - WIDTH_MENU)
#define HEIGHT_AREA HEIGHT_WINDOW

#define WIDTH_INFO WIDTH_AREA
#define HEIGHT_INFO 70

#define WIDTH_DIR 720
#define HEIGHT_DIR 480

#define WIDTH_POPUP 250
#define HEIGHT_POPUP 100
#define WIDTH_BUTTON (WIDTH_MENU / 2)
#define HEIGHT_BUTTON 40

#define BACKGROUND_COLOR_R 57
#define BACKGROUND_COLOR_G 57
#define BACKGROUND_COLOR_B 57

#define MENU_COLOR_R 24
#define MENU_COLOR_G 24
#define MENU_COLOR_B 24

#define AREA_COLOR_R BACKGROUND_COLOR_R
#define AREA_COLOR_G BACKGROUND_COLOR_G
#define AREA_COLOR_B BACKGROUND_COLOR_B

#define INFO_COLOR_R 34
#define INFO_COLOR_G 37
#define INFO_COLOR_B 41

#define SIZE_FONT_R 22
#define SIZE_FONT_B 22
#define SIZE_FONT_D 22

#define CAST_MAX 4 /* Nombre maximum de chromecast supporté par l'application */

#define DELAY_SEND_PACKET 10 /* Temps d'attente recv socket */

#include <SDL/SDL.h>

#include "ssl.h"

typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

#define h_addr h_addr_list[0]

typedef struct {
    char hostname[16];
} Connection;

typedef struct {
    char url[255];
    char mimetype[255];
    int width;
    int height;
    int depth;
} Icon;

typedef struct {
    int port;
    char url[128];
    char ip[39];
    char path[128];
    char name[128];
    char manufacturer[128];
    char model[128];
    char uuid[64];
    Icon icon;
} Protocol;

#endif /* __CONSTANTS_H */

