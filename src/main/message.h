#ifndef __MESSAGE_H
#define __MESSAGE_H

#include <stdint.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#define TYPE_ENUM 0
#define TYPE_STRING 2
#define TYPE_BYTES TYPE_STRING

#define PROTOCOL_VERSION 0
#define PAYLOAD_TYPE 0 /* String */

int getType(int fieldId, int t);
unsigned pack_B(uint8_t* buf , unsigned offset , uint8_t v );
uint8_t *getLenOf(const char *s);
unsigned pack_pct_ds_glo(uint8_t* buf , unsigned offset, uint8_t *v );
unsigned pack_I( uint8_t* buf , unsigned offset , uint32_t v );
char *nbrJSON(char *haystack, char *needle);
char *attrJSON(char *haystack, char *needle);
void requestWOAnswer(SSL *ssl, char *namespace, char *data, char *destination_id);
uint8_t *requestWAnswer(SSL *ssl, char *namespace, char *req, char *destination_id, int x, int *ind);

#endif /* __MESSAGE_H */

