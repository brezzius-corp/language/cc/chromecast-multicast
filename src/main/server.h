#ifndef __SERVER_H
#define __SERVER_H

#include "constants.h"

#define PORT_MIN_VIDEO 10240
#define PORT_MAX_VIDEO 19999

#define PORT_MIN_STREAM 20001
#define PORT_MAX_STREAM 39999

char *getHostname();
char *getIp();
int sendResponse(SOCKET csock, const char *url);
char *getUrl(const char *buffer);
void *server();

#endif /* __SERVER_H */

