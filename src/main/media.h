#ifndef __MEDIA_H
#define __MEDIA_H

typedef struct {
    char playerState[255];
    double currentTime;
    int volumeLevel;
    int volumeMute;
    int width;
    int height;
    char hdrType[255];
    double duration;
    char time[255];
} Media;

typedef struct
{
    char *path;
    char *ip;
    char *port;
    char type[1]; /* 0 : Vidéo - 1 : Streaming */
} Thread_Launch;

void *launch_video(void *p);
void *launch_stream(void *p);
void *launch_ffmpeg();
void *stream_ffmpeg(void *p);
void play(char *ip, char *path);
void stream(char *ip, int type);
char *formatTime(double time);

#endif /* __MEDIA_H */
