# Makefile

CC=gcc
CFLAGS=-W -Wall -Wextra -Werror -pedantic -ansi
LDFLAGS=-lpthread -lssl -lcrypto -lSDL -lSDL_image -lSDL_ttf
EXECUTABLE=cast
EXECUTABLE_TEST=cast_test
SRC_MAIN=src/main
SRC_TEST=src/test
SRC_BIN=bin

all: package test

package: cast
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE) $(SRC_BIN)/main.o $(SRC_BIN)/function.o $(SRC_BIN)/events.o $(SRC_BIN)/gfx.o $(SRC_BIN)/request.o $(SRC_BIN)/folder.o $(SRC_BIN)/media.o $(SRC_BIN)/server.o $(SRC_BIN)/ssl.o $(SRC_BIN)/chromecast.o $(SRC_BIN)/message.o $(SRC_BIN)/input.o $(LDFLAGS)

test: package cast_test
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE_TEST) $(SRC_BIN)/main.o

cast: init main.o function.o events.o gfx.o request.o folder.o media.o server.o ssl.o chromecast.o message.o input.o
	$(CC) -o $(SRC_BIN)/cast $(SRC_BIN)/main.o $(SRC_BIN)/function.o $(SRC_BIN)/events.o $(SRC_BIN)/gfx.o $(SRC_BIN)/request.o $(SRC_BIN)/folder.o $(SRC_BIN)/media.o $(SRC_BIN)/server.o $(SRC_BIN)/ssl.o $(SRC_BIN)/chromecast.o $(SRC_BIN)/message.o $(SRC_BIN)/input.o $(LDFLAGS)

cast_test: $(SRC_TEST)/main.c
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_TEST)/main.c $(CFLAGS)

init:
	mkdir -p bin

main.o: $(SRC_MAIN)/main.c $(SRC_MAIN)/constants.h $(SRC_MAIN)/function.h $(SRC_MAIN)/events.h $(SRC_MAIN)/request.h $(SRC_MAIN)/folder.h $(SRC_MAIN)/media.h $(SRC_MAIN)/chromecast.h $(SRC_MAIN)/server.h
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_MAIN)/main.c $(CFLAGS)

function.o: $(SRC_MAIN)/function.c $(SRC_MAIN)/function.h $(SRC_MAIN)/constants.h $(SRC_MAIN)/ssl.h
	$(CC) -o $(SRC_BIN)/function.o -c $(SRC_MAIN)/function.c $(CFLAGS)

events.o: $(SRC_MAIN)/events.c $(SRC_MAIN)/events.h
	$(CC) -o $(SRC_BIN)/events.o -c $(SRC_MAIN)/events.c $(CFLAGS)

gfx.o: $(SRC_MAIN)/gfx.c $(SRC_MAIN)/gfx.h $(SRC_MAIN)/constants.h $(SRC_MAIN)/server.h
	$(CC) -o $(SRC_BIN)/gfx.o -c $(SRC_MAIN)/gfx.c $(CFLAGS)

request.o: $(SRC_MAIN)/request.c $(SRC_MAIN)/request.h $(SRC_MAIN)/constants.h
	$(CC) -o $(SRC_BIN)/request.o -c $(SRC_MAIN)/request.c $(CFLAGS)

folder.o: $(SRC_MAIN)/folder.c $(SRC_MAIN)/folder.h $(SRC_MAIN)/constants.h $(SRC_MAIN)/input.h $(SRC_MAIN)/events.h
	$(CC) -o $(SRC_BIN)/folder.o -c $(SRC_MAIN)/folder.c $(CFLAGS)

media.o: $(SRC_MAIN)/media.c $(SRC_MAIN)/media.h $(SRC_MAIN)/server.h $(SRC_MAIN)/ssl.h $(SRC_MAIN)/chromecast.h $(SRC_MAIN)/message.h $(SRC_MAIN)/function.h
	$(CC) -o $(SRC_BIN)/media.o -c $(SRC_MAIN)/media.c $(CFLAGS)

server.o: $(SRC_MAIN)/server.c $(SRC_MAIN)/server.h $(SRC_MAIN)/constants.h
	$(CC) -o $(SRC_BIN)/server.o -c $(SRC_MAIN)/server.c $(CFLAGS)

ssl.o: $(SRC_MAIN)/ssl.c $(SRC_MAIN)/ssl.h $(SRC_MAIN)/constants.h
	$(CC) -o $(SRC_BIN)/ssl.o -c $(SRC_MAIN)/ssl.c $(CFLAGS)

chromecast.o: $(SRC_MAIN)/chromecast.c $(SRC_MAIN)/chromecast.h $(SRC_MAIN)/constants.h $(SRC_MAIN)/ssl.h $(SRC_MAIN)/message.h
	$(CC) -o $(SRC_BIN)/chromecast.o -c $(SRC_MAIN)/chromecast.c $(CFLAGS)

message.o: $(SRC_MAIN)/message.c $(SRC_MAIN)/message.h $(SRC_MAIN)/chromecast.h
	$(CC) -o $(SRC_BIN)/message.o -c $(SRC_MAIN)/message.c $(CFLAGS)

input.o: $(SRC_MAIN)/input.c $(SRC_MAIN)/input.h $(SRC_MAIN)/events.h
	$(CC) -o $(SRC_BIN)/input.o -c $(SRC_MAIN)/input.c $(CFLAGS)

clean:
	rm -f $(SRC_BIN)/*.o
	rm -f $(SRC_BIN)/$(EXECUTABLE)

