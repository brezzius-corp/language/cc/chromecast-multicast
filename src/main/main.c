#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>

#include "constants.h"
#include "function.h"
#include "events.h"
#include "request.h"
#include "folder.h"
#include "media.h"
#include "chromecast.h"
#include "server.h"

int NUM_DEVICES = -1;
Media MEDIA;

int main(int argc, char *argv[])
{
    /* Global Variables */
    int current = -1, playing = 0, playerstate = 1, i;
    char *path;
    char rscPath[255];
    pthread_t thread, sthread;
    struct timeval ta, tb;
    Protocol *dial;

    /* SDL Variables */
    SDL_Surface *screen, *menu, *area, *info, *progress, *bprogress;
    Surface welcom, tinfo;
    Image iconPlay, iconPause;
    SDL_Rect pmenu = {0, 0, 0, 0}, parea = {WIDTH_MENU, 0, 0, 0}, pinfo = {0, HEIGHT_MENU - HEIGHT_INFO, 0, 0};
    SDL_Rect pprogress = {10, 10, 0, 0}, pbprogress = {1, 1, 0, 0};
    Image search;
    GFX_Client client;
    SDL_Color white = {233, 233, 233, 255};
    TTF_Font *font[90];
    Input in;

    GIF_Image sclient;
    GFX_Chromecast *cast;
    Chromecast *castinfo;

    ThUpdateMedia thum;

    if(argc < 1) {
        fprintf(stderr, "Bad format\n");
        exit(EXIT_FAILURE);
    }

    for(i = strlen(argv[0]) - 1 ; i >= 0 ; i--) {
        if(argv[0][i] != '/')
            argv[0][i] = '\0';
        else
            break;
    }

    /* Initialisation de la SDL */
    if(SDL_Init(SDL_INIT_VIDEO))
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    /* Initialisation de la TTF */
    if(TTF_Init())
    {
        fprintf(stderr, "%s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }

    /* Création d'une fenêtre */
    if(!(screen = SDL_SetVideoMode(WIDTH_WINDOW, HEIGHT_WINDOW, 32, SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_FULLSCREEN)))
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    /* Chargement de la police */
    for(i = 0 ; i < 90 ; i++)
    {
        sprintf(rscPath, "%s/../src/main/resources/font/LinLibertine/LinLibertine_R.ttf", argv[0]);

        if(!(font[i+1] = TTF_OpenFont(rscPath, i)))
        {
            fprintf(stderr, "Echec pour %d %s\n", i, TTF_GetError());
            exit(EXIT_FAILURE);
        }
    }

    /* Chargement des images Play / Pause */
    sprintf(rscPath, "%s/../src/main/resources/image/cast_icon_resume.png", argv[0]);
    if(!(iconPlay.surf = IMG_Load(rscPath)))
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    sprintf(rscPath, "%s/../src/main/resources/image/cast_icon_pause.png", argv[0]);
    if(!(iconPause.surf = IMG_Load(rscPath)))
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    /* Titre et Icone de la fenêtre */
    sprintf(rscPath, "%s/../src/main/resources/image/icon.png", argv[0]);
    SDL_WM_SetCaption("Chromecast", rscPath);

    /* Zones de travaille */
    menu = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_MENU, HEIGHT_MENU, 32, 0, 0, 0, 0);
    area = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_AREA, HEIGHT_AREA, 32, 0, 0, 0, 0);
    info = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_INFO, HEIGHT_INFO, 32, 0, 0, 0, 0);

    /* Créaton de la surface welcom */
    sprintf(welcom.text, "CHROMECAST");
    TTF_SizeText(font[32], welcom.text, &(welcom.width), &(welcom.height));
    welcom.surf = TTF_RenderUTF8_Blended(font[32], welcom.text, white);
    welcom.rect.x = WIDTH_MENU / 2 - welcom.surf->w / 2;
    welcom.rect.y = 40;
    
    /* Création des surfaces du client et des chromecast */
    sprintf(rscPath, "%s/../src/main/resources/image/client.png", argv[0]);
    if(!(client.img.surf = IMG_Load(rscPath)))
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    client.img.rect.x = WIDTH_AREA / 2 - client.img.surf->w / 2;
    client.img.rect.y = HEIGHT_AREA / 2 - client.img.surf->h / 2;

    GFX_InitClient(&client, font);
    sclient = GIF_Init(4, "client_", client.img.rect, argv[0]);

    cast = GFX_InitChromecast(argv[0]);
    castinfo = initChromecast();

    /* Création de la surface search */
    sprintf(rscPath, "%s/../src/main/resources/image/search.png", argv[0]);
    if(!(search.surf = IMG_Load(rscPath)))
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    search.rect.x = WIDTH_MENU / 2 - search.surf->w / 2;
    search.rect.y = 150;

    /* Initialisation du générateur de nombre */
    srand(time(NULL));

    /* Allocation de dial */
    if(!(dial = malloc(CAST_MAX * sizeof(Protocol))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    /* Allocation de path */
    if(!(path = malloc(255 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    /* Initialisation des structures de chromecast */
    for(i = 0 ; i < CAST_MAX ; i++)
        memset(&(dial[i]), 0, sizeof(dial[i]));

    memset(&MEDIA, 0, sizeof(MEDIA));
    memset(&in, 0, sizeof(in));

    while(!in.quit)
    {
        /* Gestion des évènements */
        UpdateEvents(&in);

        /* Nettoyage de l'écran */
        SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, BACKGROUND_COLOR_R, BACKGROUND_COLOR_G, BACKGROUND_COLOR_B));

        /* Nettoyage des surfaces */
        SDL_FillRect(menu, NULL, SDL_MapRGB(menu->format, MENU_COLOR_R, MENU_COLOR_G, MENU_COLOR_B));
        SDL_FillRect(area, NULL, SDL_MapRGB(area->format, AREA_COLOR_R, AREA_COLOR_G, AREA_COLOR_B));
        SDL_FillRect(info, NULL, SDL_MapRGB(info->format, INFO_COLOR_R, INFO_COLOR_G, INFO_COLOR_B));

        /* Blit du nom de l'application et de la loupe */
        SDL_BlitSurface(welcom.surf, NULL, menu, &(welcom.rect));
        SDL_BlitSurface(search.surf, NULL, menu, &(search.rect));

        /* Mise à jour du status du chromecast */
        if(NUM_DEVICES > 0)
            GFX_UpdateReceiver(castinfo, cast, NUM_DEVICES, font, argv[0]);

        /* Blit des infos du chromecast sélectionné */
        if(current >= 0)
        {
            SDL_BlitSurface(cast[current].name.surf, NULL, menu, &(cast[current].name.rect));
            SDL_BlitSurface(cast[current].manufacturer.surf, NULL, menu, &(cast[current].manufacturer.rect));
            SDL_BlitSurface(cast[current].model.surf, NULL, menu, &(cast[current].model.rect));
            SDL_BlitSurface(cast[current].ip.surf, NULL, menu, &(cast[current].ip.rect));
            SDL_BlitSurface(cast[current].state.surf, NULL, menu, &(cast[current].state.rect));
            SDL_BlitSurface(cast[current].reboot.button, NULL, menu, &(cast[current].reboot.rect));
            SDL_BlitSurface(cast[current].launch.button, NULL, menu, &(cast[current].launch.rect));
            SDL_BlitSurface(cast[current].video.button, NULL, menu, &(cast[current].video.rect));
            SDL_BlitSurface(cast[current].stream.button, NULL, menu, &(cast[current].stream.rect));
            SDL_BlitSurface(cast[current].webcam.button, NULL, menu, &(cast[current].webcam.rect));
            SDL_BlitSurface(cast[current].stop.button, NULL, menu, &(cast[current].stop.rect));
            SDL_BlitSurface(cast[current].reset.button, NULL, menu, &(cast[current].reset.rect));
        }

        /* Blit des infos du client */
        if(current == -2)
        {
            SDL_BlitSurface(client.name.surf, NULL, menu, &(client.name.rect));
            SDL_BlitSurface(client.ip.surf, NULL, menu, &(client.ip.rect));
            SDL_BlitSurface(client.reboot.button, NULL, menu, &(client.reboot.rect));
            SDL_BlitSurface(client.launch.button, NULL, menu, &(client.launch.rect));
            SDL_BlitSurface(client.video.button, NULL, menu, &(client.video.rect));
            SDL_BlitSurface(client.stream.button, NULL, menu, &(client.stream.rect));
            SDL_BlitSurface(client.webcam.button, NULL, menu, &(client.webcam.rect));
            SDL_BlitSurface(client.stop.button, NULL, menu, &(client.stop.rect));
            SDL_BlitSurface(client.reset.button, NULL, menu, &(client.reset.rect));
        }

        /* Construction de la bar d'info du média */
        if(playing)
        {
            sprintf(tinfo.text, "%s / %s", formatTime(MEDIA.currentTime*1000), formatTime(MEDIA.duration*1000));
            TTF_SizeText(font[20], tinfo.text, &(tinfo.width), &(tinfo.height));
            tinfo.surf = TTF_RenderUTF8_Blended(font[20], tinfo.text, white);

            tinfo.rect.x = 10;
            tinfo.rect.y = HEIGHT_INFO - tinfo.height - 10;

            iconPlay.rect.x = tinfo.rect.x + tinfo.surf->w + 10;
            iconPlay.rect.y = tinfo.rect.y;

            iconPause.rect.x = tinfo.rect.x + tinfo.surf->w + 10;
            iconPause.rect.y = tinfo.rect.y;

            progress = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_INFO - 50, 15, 32, 0, 0, 0, 0);

            if(MEDIA.currentTime && MEDIA.duration)
            {
                bprogress = SDL_CreateRGBSurface(SDL_HWSURFACE, (MEDIA.currentTime * progress->w) / (MEDIA.duration), progress->h - 2, 32, 0, 0, 0, 0);
                SDL_FillRect(bprogress, NULL, SDL_MapRGB(bprogress->format, 0, 0, 255));

                SDL_BlitSurface(bprogress, NULL, progress, &pbprogress);
            }

            if(playerstate)
                SDL_BlitSurface(iconPause.surf, NULL, info, &(iconPause.rect));
            else
                SDL_BlitSurface(iconPlay.surf, NULL, info, &(iconPlay.rect));

            SDL_BlitSurface(progress, NULL, info, &pprogress);
            SDL_BlitSurface(tinfo.surf, NULL, info, &tinfo.rect);
            SDL_BlitSurface(info, NULL, area, &pinfo);

            SDL_FreeSurface(tinfo.surf);
            SDL_FreeSurface(progress);
        }

        SDL_BlitSurface(client.img.surf, NULL, area, &(client.img.rect));

        for(i = 0 ; i < NUM_DEVICES ; i++)
            SDL_BlitSurface(cast[i].img.surf, NULL, area, &(cast[i].img.rect));

        SDL_BlitSurface(menu, NULL, screen, &pmenu);
        SDL_BlitSurface(area, NULL, screen, &parea);

        /* Appuie sur la touche q - Quitte le programme */
        if(in.key[SDLK_q])
            in.quit = 1;

        if(in.key[SDLK_LCTRL] && in.key[SDLK_w])
        {
            in.key[SDLK_LCTRL] = 0;
            in.key[SDLK_LSHIFT] = 0;
            in.key[SDLK_w] = 0;
            SDL_WM_IconifyWindow();
        }

        if(in.key[SDLK_f])
        {
            in.key[SDLK_f] = 0;
            SDL_WM_ToggleFullScreen(screen);
        }

        /* Appuie sur la touche r / Clique sur la loupe - Lance une recherche Chromecast */
        if((in.key[SDLK_r]) || ((in.mousex > search.rect.x) && (in.mousex < search.rect.x + search.surf->w) && (in.mousey > search.rect.y) && (in.mousey < search.rect.y + search.surf->h) && (in.mousebuttons[SDL_BUTTON_LEFT])))
        {
            in.key[SDLK_LCTRL] = 0;
            in.key[SDLK_r] = 0;
            in.mousebuttons[SDL_BUTTON_LEFT] = 0;
            NUM_DEVICES = -1;

            /* Lancement du thread de recherche de chromecast */
            if(pthread_create(&thread, NULL, discoverDevices, dial))
            {
                fprintf(stderr, "Memory error\n");
                exit(EXIT_FAILURE);
            }

            gettimeofday(&tb, NULL);
            sclient.current = 0;
            do
            {
                gettimeofday(&ta, NULL);
                if((ta.tv_sec*1e6 + ta.tv_usec) - (tb.tv_sec*1e6 + tb.tv_usec) > 600000)
                {
                    tb = ta;
                    if(sclient.current < 3)
                        sclient.current++;
                    else
                        sclient.current = 0;
                }

                SDL_FillRect(area, NULL, SDL_MapRGB(screen->format, BACKGROUND_COLOR_R, BACKGROUND_COLOR_G, BACKGROUND_COLOR_B));
                SDL_BlitSurface(sclient.surface[sclient.current], NULL, area, &sclient.rect);
                SDL_BlitSurface(area, NULL, screen, &parea);
                SDL_Flip(screen);
            } while(NUM_DEVICES < 0);

            /* On attend la terminaison du thread */
            pthread_join(thread, NULL);

            /* Initialisation des infos des chromecast */
            if(NUM_DEVICES)
            {
                initReceiver(castinfo, NUM_DEVICES, dial);
                GFX_InitReceiver(castinfo, cast, NUM_DEVICES, dial, font, argv[0]);
            }

            /* Prépartion de la structure - Thread updateMedia */
            thum.dial = dial;
            thum.cast = castinfo;
            thum.num = NUM_DEVICES;

            /* Lancement du thread de recherche de chromecast */
            if(pthread_create(&sthread, NULL, updateStatus, &thum))
            {
                fprintf(stderr, "Memory error\n");
                exit(EXIT_FAILURE);
            }
        }

        /* Clique sur le Client - Affiche les infos du client */
        if((in.mousex > client.img.rect.x + WIDTH_MENU) && (in.mousex < client.img.rect.x + client.img.surf->w + WIDTH_MENU) &&
          (in.mousey > client.img.rect.y) && (in.mousey < client.img.rect.y + client.img.surf->h) && in.mousebuttons[SDL_BUTTON_LEFT])
        {
            current = -2;
            SDL_FillRect(menu, NULL, SDL_MapRGB(menu->format, MENU_COLOR_R, MENU_COLOR_G, MENU_COLOR_B));

            in.mousebuttons[SDL_BUTTON_LEFT] = 0;
        }

        /* Clique sur un Chromecast - Affiche les infos du Chromecast */
        for(i = 0 ; i < NUM_DEVICES ; i++)
        {
            if((in.mousex > cast[i].img.rect.x + WIDTH_MENU) && (in.mousex < cast[i].img.rect.x + cast[i].img.surf->w + WIDTH_MENU) &&
               (in.mousey > cast[i].img.rect.y) && (in.mousey < cast[i].img.rect.y + cast[i].img.surf->h) && in.mousebuttons[SDL_BUTTON_LEFT])
            {
                current = i;
                SDL_FillRect(menu, NULL, SDL_MapRGB(menu->format, MENU_COLOR_R, MENU_COLOR_G, MENU_COLOR_B));

                in.mousebuttons[SDL_BUTTON_LEFT] = 0;
            }
        }

        /* Options sur l'affichage des infos du Chromecast */
        if(current >= 0)
        {
            /* Reboot le Chromecast */
            if(in.mousex > cast[current].reboot.rect.x && in.mousex < cast[current].reboot.rect.x + cast[current].reboot.button->w &&
               in.mousey > cast[current].reboot.rect.y && in.mousey < cast[current].reboot.rect.y + cast[current].reboot.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;
                postRequest(dial[current].ip, PORT, "/setup/reboot", "{\"params\":\"now\"}");
            }

            /* Lance le Chromecast */
            if(in.mousex > cast[current].launch.rect.x && in.mousex < cast[current].launch.rect.x + cast[current].launch.button->w &&
               in.mousey > cast[current].launch.rect.y && in.mousey < cast[current].launch.rect.y + cast[current].launch.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;
                launchApp(dial[current].ip);
            }

            /* Play le Chromecast */
            if(in.mousex > cast[current].video.rect.x && in.mousex < cast[current].video.rect.x + cast[current].video.button->w &&
               in.mousey > cast[current].video.rect.y && in.mousey < cast[current].video.rect.y + cast[current].video.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;
                strcpy(castinfo[current].sender, client.ip.text);

                if((path = directory(screen, &in, font, argv[0])))
                {
                    play(dial[current].ip, path);
                    playing = 1;
                }

            }

            /* Stream le Chromecast */
            if(in.mousex > cast[current].stream.rect.x && in.mousex < cast[current].stream.rect.x + cast[current].stream.button->w &&
               in.mousey > cast[current].stream.rect.y && in.mousey < cast[current].stream.rect.y + cast[current].stream.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;
                strcpy(castinfo[current].sender, client.ip.text);

                stream(dial[current].ip, 1);
                playing = 1;
            }

            /* Webcam le Chromecast */
            if(in.mousex > cast[current].webcam.rect.x && in.mousex < cast[current].webcam.rect.x + cast[current].webcam.button->w &&
               in.mousey > cast[current].webcam.rect.y && in.mousey < cast[current].webcam.rect.y + cast[current].webcam.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;
                strcpy(castinfo[current].sender, client.ip.text);

                stream(dial[current].ip, 0);
                playing = 1;
            }

            /* Stop le Chromecast */
            if(in.mousex > cast[current].stop.rect.x && in.mousex < cast[current].stop.rect.x + cast[current].stop.button->w &&
               in.mousey > cast[current].stop.rect.y && in.mousey < cast[current].stop.rect.y + cast[current].stop.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;
                control(dial[current].ip, "STOP");
                strcpy(castinfo[current].sender, "");
                playing = 0;
            }

            /* Reset le Chromecast */
            if(in.mousex > cast[current].reset.rect.x && in.mousex < cast[current].reset.rect.x + cast[current].reset.button->w &&
               in.mousey > cast[current].reset.rect.y && in.mousey < cast[current].reset.rect.y + cast[current].reset.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;
                postRequest(dial[current].ip, PORT, "/setup/reboot", "{\"params\":\"fdr\"}");
            }
    
            /* Clique sur le bouton Play / Pause */
            if(in.mousex > iconPlay.rect.x + WIDTH_MENU && in.mousex < iconPlay.rect.x + iconPlay.surf->w + WIDTH_MENU &&
               in.mousey > iconPlay.rect.y + HEIGHT_AREA - HEIGHT_INFO && in.mousey < iconPlay.rect.y + iconPlay.surf->h + HEIGHT_AREA - HEIGHT_INFO  &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;

                if(playerstate)
                    control(dial[current].ip, "PAUSE");
                else
                    control(dial[current].ip, "PLAY");

                playerstate = (playerstate + 1) % 2;
            }

            /* Avance à la position 20s */
            if(in.key[SDLK_RIGHT])
            {
                in.key[SDLK_RIGHT] = 0;
                setPosition(dial[current].ip, "SEEK", 40);
            }
        }

        /* Option sur l'affichage des infos du  client */
        if(current == -2)
        {
            /* Reboot le Chromecast */
            if(in.mousex > client.reboot.rect.x && in.mousex < client.reboot.rect.x + client.reboot.button->w &&
               in.mousey > client.reboot.rect.y && in.mousey < client.reboot.rect.y + client.reboot.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;

                for(i = 0 ; i < NUM_DEVICES ; i++)
                    postRequest(dial[i].ip, PORT, "/setup/reboot", "{\"params\":\"now\"}");
            }

            /* Lance les Chromecast */
            if(in.mousex > client.launch.rect.x && in.mousex < client.launch.rect.x + client.launch.button->w &&
               in.mousey > client.launch.rect.y && in.mousey < client.launch.rect.y + client.launch.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;

                for(i = 0 ; i < NUM_DEVICES ; i++)
                    launchApp(dial[i].ip);
            }

            /* Play les Chromecast */
            if(in.mousex > client.video.rect.x && in.mousex < client.video.rect.x + client.video.button->w &&
               in.mousey > client.video.rect.y && in.mousey < client.video.rect.y + client.video.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;

                if((path = directory(screen, &in, font, argv[0])))
                {
                    for(i = 0 ; i < NUM_DEVICES ; i++)
                    {
                        strcpy(castinfo[i].sender, client.ip.text);
                        play(dial[i].ip, path);
                    }

                    playing = 1;
                }
            }

            /* Stream les Chromecast */
            if(in.mousex > client.stream.rect.x && in.mousex < client.stream.rect.x + client.stream.button->w &&
               in.mousey > client.stream.rect.y && in.mousey < client.stream.rect.y + client.stream.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;

                for(i = 0 ; i < NUM_DEVICES ; i++)
                {
                    strcpy(castinfo[i].sender, client.ip.text);
                    stream(dial[i].ip, 1);
                }

                playing = 1;
            }

            /* Webcam les Chromecast */
            if(in.mousex > client.webcam.rect.x && in.mousex < client.webcam.rect.x + client.webcam.button->w &&
               in.mousey > client.webcam.rect.y && in.mousey < client.webcam.rect.y + client.webcam.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;

                for(i = 0 ; i < NUM_DEVICES ; i++)
                {
                    strcpy(castinfo[i].sender, client.ip.text);
                    stream(dial[i].ip, 0);
                }

                playing = 1;
            }

            /* Stop les Chromecast */
            if(in.mousex > client.stop.rect.x && in.mousex < client.stop.rect.x + client.stop.button->w &&
               in.mousey > client.stop.rect.y && in.mousey < client.stop.rect.y + client.stop.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;

                for(i = 0 ; i < NUM_DEVICES ; i++)
                {
                    control(dial[i].ip, "STOP");
                    strcpy(castinfo[i].sender, "");
                }

                playing = 0;
            }

            /* Reset le Chromecast */
            if(in.mousex > client.reset.rect.x && in.mousex < client.reset.rect.x + client.reset.button->w &&
               in.mousey > client.reset.rect.y && in.mousey < client.reset.rect.y + client.reset.button->h &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;

                for(i = 0 ; i < NUM_DEVICES ; i++)
                    postRequest(dial[i].ip, PORT, "/setup/reboot", "{\"params\":\"fdr\"}");
            }

            /* Clique sur le bouton Play / Pause */
            if(in.mousex > iconPlay.rect.x + WIDTH_MENU && in.mousex < iconPlay.rect.x + iconPlay.surf->w + WIDTH_MENU &&
               in.mousey > iconPlay.rect.y + HEIGHT_AREA - HEIGHT_INFO && in.mousey < iconPlay.rect.y + iconPlay.surf->h + HEIGHT_AREA - HEIGHT_INFO  &&
               in.mousebuttons[SDL_BUTTON_LEFT])
            {
                in.mousebuttons[SDL_BUTTON_LEFT] = 0;

                if(playerstate)
                    control(dial[current].ip, "PAUSE");
                else
                    control(dial[current].ip, "PLAY");

                playerstate = (playerstate + 1) % 2;
            }
        }

        SDL_Flip(screen);
    }

    /* Libération mémoire */
    for(i = 0 ; i < 4 ; i++)
        SDL_FreeSurface(sclient.surface[i]);

    GFX_FreeChromecast(cast);

    SDL_FreeSurface(welcom.surf);
    SDL_FreeSurface(client.img.surf);
    SDL_FreeSurface(search.surf);
    SDL_FreeSurface(info);
    SDL_FreeSurface(menu);
    SDL_FreeSurface(area);
    SDL_FreeSurface(screen);

    SDL_Quit();

    return EXIT_SUCCESS;
}

