#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>

#include "input.h"
#include "events.h"

/* Valeur renvoyé 1 si la chaine est validé */
int input(SDL_Surface *surface, Input *in, TTF_Font *font, SDL_Color bg, SDL_Color color, char *string, int max, int *pchar, int shown)
{
	SDL_Surface *text;
	SDL_Rect ptext = {5, 1, 0, 0};

	char *fstring;
    
    if(!(fstring = malloc(sizeof(char) * max + 1)))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    readChar(in, string, pchar, max);
    strcpy(fstring, string);

    if(strlen(fstring) <= 0)
        strcpy(fstring, string);

    /* On souhaite caché le mot de passe */
    if(shown)
    {
	    int i = 0;
        for(i = 0 ; i < (int)strlen(fstring) ; i++)
            fstring[i] = '*';
    }

    text = TTF_RenderText_Blended(font, fstring, color);
    SDL_FillRect(surface, NULL, SDL_MapRGB(surface -> format, bg.r, bg.g, bg.b));

    SDL_BlitSurface(text, NULL, surface, &ptext);

    /* Clignotement du curseur */
    if(((int)(time(NULL))%2))
        blitCursor(&ptext, font, fstring, surface, pchar);

    free(fstring);
    SDL_FreeSurface(text);

    if(in->key[SDLK_RETURN])
        return 1;
    else
        return 0;
}
 
void blitCursor(SDL_Rect *ptext, TTF_Font *font, char *string, SDL_Surface *surface, int *pchar)
{
    int lt1 = 0, lt2 = 0, lt3 = 0, lt4 = 0;
    SDL_Rect position;
    SDL_Surface *cursor = SDL_CreateRGBSurface(SDL_HWSURFACE, 1, surface->h - 10, 32, 0, 0, 0, 0);
    SDL_FillRect(cursor, NULL, SDL_MapRGB(cursor->format, 233, 233, 233));

    if(*pchar)
        position.x = ptext->x + 1;
    else
        position.x = ptext->x + 3;

    if(!TTF_SizeText(font, string, &lt2, &lt1))
    {
        if(!TTF_SizeText(font, &string[*pchar], &lt3, &lt4))
            lt1 = lt2 - lt3;

        position.x += lt1;
    }
    position.y = ptext -> y+4;
    SDL_BlitSurface(cursor, NULL, surface, &position);
}
 
void readChar(Input *in, char *chaine, int *pchar, int max)
{
	char caractereEntre = 0;
	int letterEntry = 0, diminuerPositionCaractere = 1, capital = 0, flags = 0;
	long i = 0;

    /*for(i = 0 ; i < SDLK_LAST ; i++)
    {
        if(in->key[i])
            printf("Touche frappé : %ld\n", i);
    }*/

	if(in->key[SDLK_LSHIFT])
		capital = 1;
	else
		capital = 0;

	if(in->key[SDLK_LEFT])
	{
		if(*pchar > 0)
			*pchar -= 1;
	}
	if(in->key[SDLK_RIGHT])
	{
		if(*pchar < (int)strlen(chaine))
			*pchar += 1;
	}
	if(in->key[SDLK_BACKSPACE])
	{
		if(*pchar > 0)
		{
			for(i = *pchar - 1 ; i < (int)strlen(chaine) ; i++)
			{
				if(i > -1)
					chaine[i] = chaine[i+1];
				else
					diminuerPositionCaractere = 0;
			}
			if(diminuerPositionCaractere)
				*pchar -= 1;
			else
				*pchar = 1;
		}
	}

	detecterLettre(in, &letterEntry, &caractereEntre, &capital, &flags);

    switch(flags)
    {
        /* Ctrl + u */
        case 1:
            for(i = 0 ; i < *pchar ; i++)
                chaine[i] = 0;
            *pchar = 0;
            break;
        /* Ctrl + a */
        case 2:
            *pchar = 0;
            break;
        default:
            break;
    }
    if(letterEntry == 1 && (int)strlen(chaine) < max)
    {
        insererCaractereDansChaine(pchar, chaine, caractereEntre);
	    *pchar += 1;
    }
}
 
int detecterLettre(Input *in, int *letterEntry, char *caractereEntre, int *capital, int *flags)
{
	long i = 0;

    /* Raccourcis clavier Linux */
    if(in->key[SDLK_LCTRL])
    {
        if(in->key[SDLK_u])
            *flags = 1;
        if(in->key[SDLK_a])
            *flags = 2;

        return 1; /* On sort de la boucle pour ne saisir aucun caractère */
    }

    /* Lettres sans accents minscules et majuscules */
	for(i = 0 ; i < 26 ; i++)
	{
		if(in->key[SDLK_a + i])
		{
			entrerLettre(letterEntry, 65 + i, caractereEntre);
			if(!(*capital))
			{
				*caractereEntre += 32;
			}
			break;
		}
	}

    /* Chiffre */
    if(in->key[SDLK_LSHIFT])
    {
        if(in->key[SDLK_WORLD_64])
            entrerLettre(letterEntry, 48, caractereEntre);
        if(in->key[SDLK_AMPERSAND])
            entrerLettre(letterEntry, 49, caractereEntre);
        else if(in->key[SDLK_WORLD_73])
            entrerLettre(letterEntry, 50, caractereEntre);
        else if(in->key[SDLK_QUOTEDBL])
            entrerLettre(letterEntry, 51, caractereEntre);
        else if(in->key[SDLK_QUOTE])
            entrerLettre(letterEntry, 52, caractereEntre);
        else if(in->key[SDLK_LEFTPAREN])
            entrerLettre(letterEntry, 53, caractereEntre);
        else if(in->key[SDLK_MINUS])
            entrerLettre(letterEntry, 54, caractereEntre);
        else if(in->key[SDLK_WORLD_72])
            entrerLettre(letterEntry, 55, caractereEntre);
        else if(in->key[SDLK_UNDERSCORE])
            entrerLettre(letterEntry, 56, caractereEntre);
        else if(in->key[SDLK_WORLD_71])
            entrerLettre(letterEntry, 57, caractereEntre);
        else if(in->key[SDLK_WORLD_64])
            entrerLettre(letterEntry, 58, caractereEntre);
    }

    /* Espace */
    if(in->key[SDLK_SPACE])
        entrerLettre(letterEntry, 32, caractereEntre);

    /* Ponctuation ( . ? / % ) */
    if(in->key[SDLK_LSHIFT])
    {
        if(in->key[SDLK_SEMICOLON])
            entrerLettre(letterEntry, 46, caractereEntre);
        else if(in->key[SDLK_COMMA])
            entrerLettre(letterEntry, 63, caractereEntre);
        else if(in->key[SDLK_COLON])
            entrerLettre(letterEntry, 47, caractereEntre);
        else if(in->key[SDLK_WORLD_89])
            entrerLettre(letterEntry, 37, caractereEntre);
    }

    /* Caractère ( ; , : ! ) */
    else
    {
        if(in->key[SDLK_SEMICOLON])
            entrerLettre(letterEntry, 59, caractereEntre);
        else if(in->key[SDLK_COMMA])
            entrerLettre(letterEntry, 44, caractereEntre);
        else if(in->key[SDLK_COLON])
            entrerLettre(letterEntry, 58, caractereEntre);
        else if(in->key[SDLK_EXCLAIM])
            entrerLettre(letterEntry, 33, caractereEntre);
    }

    /* Arobase */
    if(in->key[SDLK_MODE] && in->key[SDLK_WORLD_64])
        entrerLettre(letterEntry, 64, caractereEntre);

    /* Accents é è à ç ù */
    if(in->key[SDLK_WORLD_73])
        entrerLettre(letterEntry, 233, caractereEntre);
    if(in->key[SDLK_WORLD_72])
        entrerLettre(letterEntry, 232, caractereEntre);
    if(in->key[SDLK_WORLD_64])
        entrerLettre(letterEntry, 224, caractereEntre);
    if(in->key[SDLK_WORLD_71])
        entrerLettre(letterEntry, 231, caractereEntre);
    if(in->key[SDLK_WORLD_89])
        entrerLettre(letterEntry, 249, caractereEntre);

    /* Réinitialisation de la touche pressé */
    for(i = 0 ; i < SDLK_LAST ; i++)
        if(in->key[i] != in->key[SDLK_RETURN] && in->key[i] != in->key[SDLK_LSHIFT])
            in->key[i] = 0;

    return 0;
}
 
void entrerLettre(int *letterEntry, long numeroCaractere, char *caractere)
{
	if(!(*letterEntry))
	{
		*letterEntry = 1;
		*caractere = numeroCaractere;
	}
}
 
void insererCaractereDansChaine(int *pchar, char *chaine, char caractereAInserer)
{
	char chaine2[100] = "";
	strcpy(chaine2, &chaine[*pchar]);
	chaine[*pchar] = caractereAInserer;
	strcpy(&chaine[*pchar+1], chaine2);
}

