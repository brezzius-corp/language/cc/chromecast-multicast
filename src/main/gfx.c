#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>

#include "constants.h"
#include "gfx.h"
#include "server.h"

char *statusTextIdent[] = {"Déconnecté", "Prêt à Caster", "Cast en cours"};

GFX_Chromecast *GFX_InitChromecast(char *path)
{
    int i;
    char rscPath[255];
    GFX_Chromecast *cast;

    /* Allocation des cast */
    if(!(cast = malloc(CAST_MAX * sizeof(GFX_Chromecast))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    /* Chargement des images */
    for(i = 0 ; i < CAST_MAX ; i++)
    {
        sprintf(rscPath, "%s/../src/main/resources/image/cast.png", path);
        if(!(cast[i].img.surf = IMG_Load(rscPath)))
        {
            fprintf(stderr, "%s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }
    }
    cast[0].img.rect.x = WIDTH_AREA - cast[0].img.surf->w - 105;
    cast[0].img.rect.y = HEIGHT_AREA / 2 - cast[0].img.surf->h / 2;
    cast[1].img.rect.x = 150;
    cast[1].img.rect.y = HEIGHT_AREA / 2 - cast[1].img.surf->h / 2;
    cast[2].img.rect.x = WIDTH_AREA / 2 - cast[2].img.surf->w / 2;
    cast[2].img.rect.y = HEIGHT_AREA - cast[2].img.surf->h - 50;
    cast[3].img.rect.x = WIDTH_AREA / 2 - cast[3].img.surf->w / 2;
    cast[3].img.rect.y = 50 + cast[3].img.surf->h;

    return cast;
}

void GFX_FreeChromecast(GFX_Chromecast *cast)
{
    int i;
    for(i = 0 ; i < CAST_MAX ; i++)
        SDL_FreeSurface(cast[i].img.surf);
}

GIF_Image GIF_Init(int len, char *path, SDL_Rect rect, char *cwpath)
{
    int i;
    char rscPath[255];
    GIF_Image client;

    /* Création des surfaces images Gif */
    if(!(client.surface = malloc(len * sizeof(SDL_Surface *))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    for(i = 0 ; i < 4 ; i++)
    {
        sprintf(rscPath, "%s/../src/main/resources/image/%s%d.png", cwpath, path, i);
        if(!(client.surface[i] = IMG_Load(rscPath)))
        {
            fprintf(stderr, "%s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }
    }

    client.rect.x = rect.x;
    client.rect.y = rect.y;

    return client;
}

void GFX_InitReceiver(Chromecast *castinfo, GFX_Chromecast *cast, int num, Protocol *dial, TTF_Font **font, char *path)
{
    int i;
    char rscPath[255];
    SDL_Color white = {233, 233, 233, 255};

    /* Initialisation des infos des chromecast */
    if(num)
    {
        for(i = 0 ; i < num ; i++)
        {
            /* Free des anciennes variables allouées */
  /*          if(cast[i].name.surf)
                SDL_FreeSurface(cast[i].name.surf);

            if(cast[i].ip.surf)
                SDL_FreeSurface(cast[i].ip.surf);

            if(cast[i].reboot.button)
                SDL_FreeSurface(cast[i].reboot.button);

            if(cast[i].launch.button)
                SDL_FreeSurface(cast[i].launch.button);

            if(cast[i].video.button)
                SDL_FreeSurface(cast[i].video.button);

            if(cast[i].stream.button)
                SDL_FreeSurface(cast[i].stream.button);

            if(cast[i].webcam.button)
                SDL_FreeSurface(cast[i].webcam.button);

            if(cast[i].stop.button)
                SDL_FreeSurface(cast[i].stop.button);
*/
            /* Images */
            switch(castinfo[i].state)
            {
                case 0:
                    SDL_FreeSurface(cast[i].img.surf);
                    sprintf(rscPath, "%s/../src/main/resources/image/cast.png", path);
                    if(!(cast[i].img.surf = IMG_Load(rscPath)))
                    {
                        fprintf(stderr, "%s\n", SDL_GetError());
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 1:
                    SDL_FreeSurface(cast[i].img.surf);
                    sprintf(rscPath, "%s/../src/main/resources/image/cast_on2.png", path);
                    if(!(cast[i].img.surf = IMG_Load(rscPath)))
                    {
                        fprintf(stderr, "%s\n", SDL_GetError());
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 2 :
                    SDL_FreeSurface(cast[i].img.surf);

                    if(strcmp(castinfo[i].sender, ""))
                    {
                        sprintf(rscPath, "%s/../src/main/resources/image/cast_active2.png", path);
                        if(!(cast[i].img.surf = IMG_Load(rscPath)))
                        {
                            fprintf(stderr, "%s\n", SDL_GetError());
                            exit(EXIT_FAILURE);
                        }
                    }
                    else
                    {
                        sprintf(rscPath, "%s/../src/main/resources/image/cast_warning2.png", path);
                        if(!(cast[i].img.surf = IMG_Load(rscPath)))
                        {
                            fprintf(stderr, "%s\n", SDL_GetError());
                            exit(EXIT_FAILURE);
                        }
                    }
                    break;
                default:
                    break;
            }

            /* Nom */
            sprintf(cast[i].name.text, "Nom : %s", dial[i].name);
            TTF_SizeText(font[SIZE_FONT_R], cast[i].name.text, &(cast[i].name.width), &(cast[i].name.height));
            cast[i].name.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_R], cast[i].name.text, white);
            cast[i].name.rect.x = 20;
            cast[i].name.rect.y = 300;

            /* Fabricant */
            sprintf(cast[i].manufacturer.text, "Fabricant : %s", dial[i].manufacturer);
            TTF_SizeText(font[SIZE_FONT_R], cast[i].manufacturer.text, &(cast[i].manufacturer.width), &(cast[i].manufacturer.height));
            cast[i].manufacturer.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_R], cast[i].manufacturer.text, white);
            cast[i].manufacturer.rect.x = 20;
            cast[i].manufacturer.rect.y = cast[i].name.rect.y + cast[i].name.height + 20;

            /* Modèle */
            sprintf(cast[i].model.text, "Modele : %s", dial[i].model);
            TTF_SizeText(font[SIZE_FONT_R], cast[i].model.text, &(cast[i].model.width), &(cast[i].model.height));
            cast[i].model.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_R], cast[i].model.text, white);
            cast[i].model.rect.x = 20;
            cast[i].model.rect.y = cast[i].manufacturer.rect.y + cast[i].manufacturer.height + 20;

            /* Adresse IP */
            sprintf(cast[i].ip.text, "IP : %s", dial[i].ip);
            TTF_SizeText(font[SIZE_FONT_R], cast[i].ip.text, &(cast[i].ip.width), &(cast[i].ip.height));
            cast[i].ip.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_R], cast[i].ip.text, white);
            cast[i].ip.rect.x = 20;
            cast[i].ip.rect.y = cast[i].model.rect.y + cast[i].model.height + 20;

            /* Etat */
            sprintf(cast[i].state.text, "Etat : %s", statusTextIdent[castinfo[i].state]);
            TTF_SizeText(font[SIZE_FONT_R], cast[i].state.text, &(cast[i].state.width), &(cast[i].state.height));
            cast[i].state.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_R], cast[i].state.text, white);
            cast[i].state.rect.x = 20;
            cast[i].state.rect.y = cast[i].ip.rect.y + cast[i].ip.height + 20;

            /* Bouton LAUNCH */
            sprintf(cast[i].launch.text, "LAUNCH");
            TTF_SizeText(font[SIZE_FONT_B], cast[i].launch.text, &(cast[i].launch.width), &(cast[i].launch.height));
            cast[i].launch.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], cast[i].launch.text, white);
            cast[i].launch.rect.x = 20;
            cast[i].launch.rect.y = cast[i].state.rect.y + cast[i].state.height + 20;
            cast[i].launch.psurf.x = 10;
            cast[i].launch.psurf.y = HEIGHT_BUTTON / 2 - cast[i].launch.height / 2;

            cast[i].launch.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
            SDL_FillRect(cast[i].launch.button, NULL, SDL_MapRGB(cast[i].launch.button->format, 120, 120, 120));
            SDL_BlitSurface(cast[i].launch.surf, NULL, cast[i].launch.button, &(cast[i].launch.psurf));

            /* Bouton VIDEO */
            sprintf(cast[i].video.text, "VIDEO");
            TTF_SizeText(font[SIZE_FONT_B], cast[i].video.text, &(cast[i].video.width), &(cast[i].video.height));
            cast[i].video.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], cast[i].video.text, white);
            cast[i].video.rect.x = 20;
            cast[i].video.rect.y = cast[i].launch.rect.y + cast[i].launch.height + 20;
            cast[i].video.psurf.x = 10;
            cast[i].video.psurf.y = HEIGHT_BUTTON / 2 - cast[i].video.height / 2;

            cast[i].video.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
            SDL_FillRect(cast[i].video.button, NULL, SDL_MapRGB(cast[i].video.button->format, 120, 120, 120));
            SDL_BlitSurface(cast[i].video.surf, NULL, cast[i].video.button, &(cast[i].video.psurf));

            /* Bouton STREAM */
            sprintf(cast[i].stream.text, "STREAM");
            TTF_SizeText(font[SIZE_FONT_B], cast[i].stream.text, &(cast[i].stream.width), &(cast[i].stream.height));
            cast[i].stream.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], cast[i].stream.text, white);
            cast[i].stream.rect.x = 20;
            cast[i].stream.rect.y = cast[i].video.rect.y + cast[i].video.height + 20;
            cast[i].stream.psurf.x = 10;
            cast[i].stream.psurf.y = HEIGHT_BUTTON / 2 - cast[i].stream.height / 2;

            cast[i].stream.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
            SDL_FillRect(cast[i].stream.button, NULL, SDL_MapRGB(cast[i].stream.button->format, 120, 120, 120));
            SDL_BlitSurface(cast[i].stream.surf, NULL, cast[i].stream.button, &(cast[i].stream.psurf));

            /* Bouton WEBCAM */
            sprintf(cast[i].webcam.text, "WEBCAM");
            TTF_SizeText(font[SIZE_FONT_B], cast[i].webcam.text, &(cast[i].webcam.width), &(cast[i].webcam.height));
            cast[i].webcam.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], cast[i].webcam.text, white);
            cast[i].webcam.rect.x = 20;
            cast[i].webcam.rect.y = cast[i].stream.rect.y + cast[i].stream.height + 20;
            cast[i].webcam.psurf.x = 10;
            cast[i].webcam.psurf.y = HEIGHT_BUTTON / 2 - cast[i].webcam.height / 2;

            cast[i].webcam.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
            SDL_FillRect(cast[i].webcam.button, NULL, SDL_MapRGB(cast[i].webcam.button->format, 120, 120, 120));
            SDL_BlitSurface(cast[i].webcam.surf, NULL, cast[i].webcam.button, &(cast[i].webcam.psurf));

            /* Bouton stop */
            sprintf(cast[i].stop.text, "STOP");
            TTF_SizeText(font[SIZE_FONT_B], cast[i].stop.text, &(cast[i].stop.width), &(cast[i].stop.height));
            cast[i].stop.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], cast[i].stop.text, white);
            cast[i].stop.rect.x = 20;
            cast[i].stop.rect.y = cast[i].webcam.rect.y + cast[i].webcam.height + 20;
            cast[i].stop.psurf.x = 10;
            cast[i].stop.psurf.y = HEIGHT_BUTTON / 2 - cast[i].stop.height / 2;

            cast[i].stop.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
            SDL_FillRect(cast[i].stop.button, NULL, SDL_MapRGB(cast[i].stop.button->format, 120, 120, 120));
            SDL_BlitSurface(cast[i].stop.surf, NULL, cast[i].stop.button, &(cast[i].stop.psurf));

            /* Bouton REBOOT */
            sprintf(cast[i].reboot.text, "REBOOT");
            TTF_SizeText(font[SIZE_FONT_B], cast[i].reboot.text, &(cast[i].reboot.width), &(cast[i].reboot.height));
            cast[i].reboot.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], cast[i].reboot.text, white);
            cast[i].reboot.rect.x = 20;
            cast[i].reboot.rect.y = cast[i].stop.rect.y + cast[i].stop.height + 20;
            cast[i].reboot.psurf.x = 10;
            cast[i].reboot.psurf.y = HEIGHT_BUTTON / 2 - cast[i].reboot.height / 2;

            cast[i].reboot.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
            SDL_FillRect(cast[i].reboot.button, NULL, SDL_MapRGB(cast[i].reboot.button->format, 120, 120, 120));
            SDL_BlitSurface(cast[i].reboot.surf, NULL, cast[i].reboot.button, &(cast[i].reboot.psurf));

            /* Bouton RESET */
            sprintf(cast[i].reset.text, "RESET");
            TTF_SizeText(font[SIZE_FONT_B], cast[i].reset.text, &(cast[i].reset.width), &(cast[i].reset.height));
            cast[i].reset.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], cast[i].reset.text, white);
            cast[i].reset.rect.x = 20;
            cast[i].reset.rect.y = cast[i].reboot.rect.y + cast[i].reboot.height + 20;
            cast[i].reset.psurf.x = 10;
            cast[i].reset.psurf.y = HEIGHT_BUTTON / 2 - cast[i].reset.height / 2;

            cast[i].reset.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
            SDL_FillRect(cast[i].reset.button, NULL, SDL_MapRGB(cast[i].reset.button->format, 120, 120, 120));
            SDL_BlitSurface(cast[i].reset.surf, NULL, cast[i].reset.button, &(cast[i].reset.psurf));

            SDL_FreeSurface(cast[i].reboot.surf);
            SDL_FreeSurface(cast[i].launch.surf);
            SDL_FreeSurface(cast[i].video.surf);
            SDL_FreeSurface(cast[i].stream.surf);
            SDL_FreeSurface(cast[i].webcam.surf);
            SDL_FreeSurface(cast[i].stop.surf);
            SDL_FreeSurface(cast[i].reset.surf);
        }
    }
}

void GFX_InitClient(GFX_Client *client, TTF_Font **font)
{
    SDL_Color white = {233, 233, 233, 255};

    /* Free des anciennes variables allouées */
    /*if(client->name.surf)
        SDL_FreeSurface(client->name.surf);

    if(client->ip.surf)
        SDL_FreeSurface(client->ip.surf);

    if(client->reboot.button)
        SDL_FreeSurface(client->reboot.button);

    if(client->launch.button)
        SDL_FreeSurface(client->launch.button);

    if(client->video.button)
        SDL_FreeSurface(client->video.button);

    if(client->stream.button)
        SDL_FreeSurface(client->stream.button);

    if(client->webcam.button)
        SDL_FreeSurface(client->webcam.button);

    if(client->stop.button)
        SDL_FreeSurface(client->stop.button);

    if(client->reset.button)
        SDL_FreeSurface(client->reset.button);*/

    /* Nom */
    sprintf(client->name.text, "Nom : %s", getHostname());
    TTF_SizeText(font[SIZE_FONT_R], client->name.text, &(client->name.width), &(client->name.height));
    client->name.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_R], client->name.text, white);
    client->name.rect.x = 20;
    client->name.rect.y = 300;

    /* Adresse IP */
    sprintf(client->ip.text, "IP : %s", getIp());
    TTF_SizeText(font[SIZE_FONT_R], client->ip.text, &(client->ip.width), &(client->ip.height));
    client->ip.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_R], client->ip.text, white);
    client->ip.rect.x = 20;
    client->ip.rect.y = client->name.rect.y + client->name.height + 20;

    /* Bouton LAUNCH */
    sprintf(client->launch.text, "LAUNCH");
    TTF_SizeText(font[SIZE_FONT_B], client->launch.text, &(client->launch.width), &(client->launch.height));
    client->launch.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], client->launch.text, white);
    client->launch.rect.x = 20;
    client->launch.rect.y = client->ip.rect.y + client->ip.height + 20;
    client->launch.psurf.x = 10;
    client->launch.psurf.y = HEIGHT_BUTTON / 2 - client->launch.height / 2;

    client->launch.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
    SDL_FillRect(client->launch.button, NULL, SDL_MapRGB(client->launch.button->format, 120, 120, 120));
    SDL_BlitSurface(client->launch.surf, NULL, client->launch.button, &(client->launch.psurf));

    /* Bouton VIDEO */
    sprintf(client->video.text, "VIDEO");
    TTF_SizeText(font[SIZE_FONT_B], client->video.text, &(client->video.width), &(client->video.height));
    client->video.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], client->video.text, white);
    client->video.rect.x = 20;
    client->video.rect.y = client->launch.rect.y + client->launch.height + 20;
    client->video.psurf.x = 10;
    client->video.psurf.y = HEIGHT_BUTTON / 2 - client->video.height / 2;

    client->video.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
    SDL_FillRect(client->video.button, NULL, SDL_MapRGB(client->video.button->format, 120, 120, 120));
    SDL_BlitSurface(client->video.surf, NULL, client->video.button, &(client->video.psurf));

    /* Bouton STREAM */
    sprintf(client->stream.text, "STREAM");
    TTF_SizeText(font[SIZE_FONT_B], client->stream.text, &(client->stream.width), &(client->stream.height));
    client->stream.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], client->stream.text, white);
    client->stream.rect.x = 20;
    client->stream.rect.y = client->video.rect.y + client->video.height + 20;
    client->stream.psurf.x = 10;
    client->stream.psurf.y = HEIGHT_BUTTON / 2 - client->stream.height / 2;

    client->stream.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
    SDL_FillRect(client->stream.button, NULL, SDL_MapRGB(client->stream.button->format, 120, 120, 120));
    SDL_BlitSurface(client->stream.surf, NULL, client->stream.button, &(client->stream.psurf));

    /* Bouton WEBCAM */
    sprintf(client->webcam.text, "WEBCAM");
    TTF_SizeText(font[SIZE_FONT_B], client->webcam.text, &(client->webcam.width), &(client->webcam.height));
    client->webcam.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], client->webcam.text, white);
    client->webcam.rect.x = 20;
    client->webcam.rect.y = client->stream.rect.y + client->stream.height + 20;
    client->webcam.psurf.x = 10;
    client->webcam.psurf.y = HEIGHT_BUTTON / 2 - client->webcam.height / 2;

    client->webcam.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
    SDL_FillRect(client->webcam.button, NULL, SDL_MapRGB(client->webcam.button->format, 120, 120, 120));
    SDL_BlitSurface(client->webcam.surf, NULL, client->webcam.button, &(client->webcam.psurf));

    /* Bouton STOP */
    sprintf(client->stop.text, "STOP");
    TTF_SizeText(font[SIZE_FONT_B], client->stop.text, &(client->stop.width), &(client->stop.height));
    client->stop.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], client->stop.text, white);
    client->stop.rect.x = 20;
    client->stop.rect.y = client->webcam.rect.y + client->webcam.height + 20;
    client->stop.psurf.x = 10;
    client->stop.psurf.y = HEIGHT_BUTTON / 2 - client->stop.height / 2;

    client->stop.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
    SDL_FillRect(client->stop.button, NULL, SDL_MapRGB(client->stop.button->format, 120, 120, 120));
    SDL_BlitSurface(client->stop.surf, NULL, client->stop.button, &(client->stop.psurf));

    /* Bouton REBOOT */
    sprintf(client->reboot.text, "REBOOT");
    TTF_SizeText(font[SIZE_FONT_B], client->reboot.text, &(client->reboot.width), &(client->reboot.height));
    client->reboot.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], client->reboot.text, white);
    client->reboot.rect.x = 20;
    client->reboot.rect.y = client->stop.rect.y + client->stop.height + 20;
    client->reboot.psurf.x = 10;
    client->reboot.psurf.y = HEIGHT_BUTTON / 2 - client->reboot.height / 2;

    client->reboot.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
    SDL_FillRect(client->reboot.button, NULL, SDL_MapRGB(client->reboot.button->format, 120, 120, 120));
    SDL_BlitSurface(client->reboot.surf, NULL, client->reboot.button, &(client->reboot.psurf));

    /* Bouton RESET */
    sprintf(client->reset.text, "RESET");
    TTF_SizeText(font[SIZE_FONT_B], client->reset.text, &(client->reset.width), &(client->reset.height));
    client->reset.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_B], client->reset.text, white);
    client->reset.rect.x = 20;
    client->reset.rect.y = client->reboot.rect.y + client->reboot.height + 20;
    client->reset.psurf.x = 10;
    client->reset.psurf.y = HEIGHT_BUTTON / 2 - client->reset.height / 2;

    client->reset.button = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_BUTTON, HEIGHT_BUTTON, 32, 0, 0, 0, 0);
    SDL_FillRect(client->reset.button, NULL, SDL_MapRGB(client->reset.button->format, 120, 120, 120));
    SDL_BlitSurface(client->reset.surf, NULL, client->reset.button, &(client->reset.psurf));

    SDL_FreeSurface(client->reboot.surf);
    SDL_FreeSurface(client->launch.surf);
    SDL_FreeSurface(client->video.surf);
    SDL_FreeSurface(client->stream.surf);
    SDL_FreeSurface(client->webcam.surf);
    SDL_FreeSurface(client->stop.surf);
    SDL_FreeSurface(client->reset.surf);
}

void GFX_UpdateReceiver(Chromecast *castinfo, GFX_Chromecast *cast, int num, TTF_Font **font, char *path)
{
    int i;
    char rscPath[255];
    SDL_Color white = {233, 233, 233, 255};

    /* Initialisation des infos des chromecast */
    if(num)
    {
        for(i = 0 ; i < num ; i++)
        {
            /* Images */
            switch(castinfo[i].state)
            {
                case 0:
                    SDL_FreeSurface(cast[i].img.surf);
                    sprintf(rscPath, "%s/../src/main/resources/image/cast.png", path);
                    if(!(cast[i].img.surf = IMG_Load(rscPath)))
                    {
                        fprintf(stderr, "%s\n", SDL_GetError());
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 1:
                    SDL_FreeSurface(cast[i].img.surf);
                    sprintf(rscPath, "%s/../src/main/resources/image/cast.png", path);
                    if(!(cast[i].img.surf = IMG_Load(rscPath)))
                    {
                        fprintf(stderr, "%s\n", SDL_GetError());
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 2 :
                    SDL_FreeSurface(cast[i].img.surf);

                    if(strcmp(castinfo[i].sender, ""))
                    {
                        sprintf(rscPath, "%s/../src/main/resources/image/cast_active.png", path);
                        if(!(cast[i].img.surf = IMG_Load(rscPath)))
                        {
                            fprintf(stderr, "%s\n", SDL_GetError());
                            exit(EXIT_FAILURE);
                        }
                    }
                    else
                    {
                        sprintf(rscPath, "%s/../src/main/resources/image/cast_warning.png", path);
                        if(!(cast[i].img.surf = IMG_Load(rscPath)))
                        {
                            fprintf(stderr, "%s\n", SDL_GetError());
                            exit(EXIT_FAILURE);
                        }
                    }
                    break;
                default:
                    break;
            }

            SDL_FreeSurface(cast[i].state.surf);

            /* Mise à jour de l'etat */
            sprintf(cast[i].state.text, "Etat : %s", statusTextIdent[castinfo[i].state]);
            cast[i].state.surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_R], cast[i].state.text, white);
        }
    }
}

