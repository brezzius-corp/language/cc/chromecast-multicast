#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <spawn.h>
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509_vfy.h>

#include "media.h"
#include "server.h"
#include "ssl.h"
#include "chromecast.h"
#include "message.h"
#include "function.h"

extern Media MEDIA;

void *launch_video(void *p)
{
    Thread_Launch *th = (Thread_Launch *) p;

    int port = atoi(th->port), type = atoi(th->type);
    char *data, sessionId[255], transportId[255] = "receiver-0", *ip = th->ip, *path = th->path;
    char req[255];

    VarSSL ssl = initssl(ip);

    /* Allocation de data */
    if(!(data = malloc(4096 * sizeof(uint8_t))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    /* Connexion au chromecast */
    connection(ssl.ssl, transportId);

    /* On récupère le status du chromecast */
    do {
        data = getStatus(ssl.ssl);
    } while(strstr(data, "\"PING\""));

    /* On récupère sessionId */
    if(attrJSON(data, "\"sessionId\""))
        strcpy(sessionId, attrJSON(data, "\"sessionId\""));
    else
    {
        fprintf(stderr, "Erreur lors de la connexion au chromecast\n");
        exit(EXIT_FAILURE);
    }

    /* Chromecast déjà initiliser : prêt à caster */
    if(!strcmp("Ready To Cast", attrJSON(data, "\"statusText\"")))
    {
        /* On tente de récupérer transportId */
        if(attrJSON(data, "\"transportId\""))
            strcpy(transportId, attrJSON(data, "\"transportId\""));
    }

    if(type)
        sprintf(req, "http://%s:%d/stream.webm", getIp(), port);
    else
        sprintf(req, "http://%s:%d?%s", getIp(), port, path);

    load(ssl.ssl, sessionId, transportId, req);

    SSL_free(ssl.ssl);
    close(ssl.server);
    X509_free(ssl.cert);
    SSL_CTX_free(ssl.ctx);

    memset(&MEDIA, 0, sizeof(MEDIA));

    do
    {
        updateMedia(ip);
    } while(MEDIA.currentTime < MEDIA.duration - 1 || MEDIA.duration == 0);

    return NULL;
}

void *stream_ffmpeg(void *p)
{
    int type = *((int *) p);
    char cmd[255];

    if(type)
        strcpy(cmd, "ffmpeg -ss 00:00:00 -s 1920x1080 -f X11grab -i :0.0+0,0 -copyts -c:v libvpx -b:v 4M -crf 16 -quality realtime -cpu-used 8 -c:a libvorbis -f webm ../videos/stream.webm > /dev/null 2>&1");
    else
        strcpy(cmd, "ffmpeg -s 1920x1080 -i /dev/video0 -codec:v libvpx -b:v 4M -b:a libvorbis -crf 20 ./stream.webm > /dev/null 2>&1");

    remove("./stream.webm");
    system(cmd);

    return NULL;
}

void play(char *ip, char *path)
{
    int port;
    pthread_t sthread, lthread;
    Thread_Launch *th = malloc(sizeof(Thread_Launch));

    /* Générateur de port */
    port = (rand() % (PORT_MAX_VIDEO - PORT_MIN_VIDEO + 1) + PORT_MIN_VIDEO);

    /* Lancement du serveur */
    if(pthread_create(&sthread, NULL, server, &port))
    {
        fprintf(stderr, "Impossible d'initialiser le serveur\n");
        exit(EXIT_FAILURE);
    }

    msleep(1000);

    /* Allocation du port */
    if(!(th->port = malloc(5 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    /* Configuration du Thread */
    th->path = path;
    th->ip = ip;
    sprintf(th->port, "%d", port);
    strcpy(th->type, "0");

    /* Lancement du media */
    if(pthread_create(&lthread, NULL, launch_video, th))
    {
        fprintf(stderr, "Impossible d'initialiser le serveur\n");
        exit(EXIT_FAILURE);
    }

    msleep(1000);
}

void stream(char *ip, int type)
{
    int port;
    pthread_t fthread, sthread, lthread;
    Thread_Launch *th = malloc(sizeof(Thread_Launch));

    /* Générateur de port */
    port = (rand() % (PORT_MAX_STREAM - PORT_MIN_STREAM + 1) + PORT_MIN_STREAM);

    /* Lancement du serveur */
    if(pthread_create(&sthread, NULL, server, &port))
    {
        fprintf(stderr, "Impossible d'initialiser le serveur\n");
        exit(EXIT_FAILURE);
    }

    msleep(1000);

    /* Lancement du stream - FFmpeg */
    if(pthread_create(&fthread, NULL, stream_ffmpeg, &type))
    {
        fprintf(stderr, "Impossible d'initialiser le serveur\n");
        exit(EXIT_FAILURE);
    }

    /* Allocation du port */
    if(!(th->port = malloc(5 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    /* Configuration du Thread */
    th->ip = ip;
    sprintf(th->port, "%d", port);
    sprintf(th->type, "1");

    /* Lancement du media */
    if(pthread_create(&lthread, NULL, launch_video, th))
    {
        fprintf(stderr, "Impossible d'initialiser le serveur\n");
        exit(EXIT_FAILURE);
    }

    msleep(1000);
}

char *formatTime(double time)
{
    unsigned int len = time, ms = 0, sec = 0, min = 0;

    char *text;
    
    if(!(text = malloc(19 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    ms = len%1000;
    sec = len/1000;

    if(sec >= 60)
    {
        min = sec/60;
        sec = sec%60;
    }

    sprintf(text, "%d:%.2d.%.3d", min, sec, ms);

    return text;
}

