#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#include "message.h"
#include "chromecast.h"

char src[] = "sender-0";

int getType(int fieldId, int t)
{
    return (fieldId << 3) | t;
}

unsigned pack_B(uint8_t* buf , unsigned offset , uint8_t v )
{
    buf[offset] = v;
    return offset + 1;
}

uint8_t *getLenOf(const char *s)
{
    uint8_t *x = malloc(sizeof(uint8_t));
    int len = (int) strlen(s), offset = 0;

    while(len > 0x7F)
    {
        offset = pack_B(x, offset, (len & 0x7F) | 0x80);
        len >>= 7;
        x = realloc(x, (offset + 1) * sizeof(uint8_t));
    }

    offset = pack_B(x, offset, len & 0x7F);
    x = realloc(x, (offset + 1) * sizeof(uint8_t));
    x[offset] = 0;

    return x;
}

unsigned pack_pct_ds(uint8_t* buf , unsigned offset, char const *v )
{
    size_t lg = strlen(v);
    memcpy(buf + offset, v, lg);
    return offset + lg;
}

unsigned pack_pct_ds_glo(uint8_t* buf , unsigned offset, uint8_t *v )
{
    size_t lg = strlen((char *) v);

    memcpy(buf + offset, v, lg);
    return offset + lg;
}

unsigned pack_I( uint8_t* buf , unsigned offset , uint32_t v )
{
    buf[offset] = v >> 24;
    buf[offset + 1] = 0xff & (v >> 16);
    buf[offset + 2] = 0xff & (v >> 8);
    buf[offset + 3] = 0xff & (v - 4);
    return offset;
}

char *nbrJSON(char *haystack, char *needle)
{
    char *attr, *ret;
    int i = 0;

    /* Allocation de attr */
    if(!(attr = malloc(255 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    if(!(ret = (strstr(haystack, needle))))
        return NULL;
    else
        ret+=strlen(needle)+1;

    while((ret[i] >= 48 && ret[i] <= 57) || ret[i] == 46)
    {
        attr[i] = ret[i];
        i++;
    }

    attr[i] = '\0';

    return attr;
}

char *attrJSON(char *haystack, char *needle)
{
    char *attr, *ret;
    int i = 0;

    /* Allocation de attr */
    if(!(attr = malloc(255 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    if(!(ret = (strstr(haystack, needle))))
        return NULL;
    else
        ret+=strlen(needle) + 2;

    while(ret[i] != '"')
    {
        attr[i] = ret[i];
        i++;
    }

    attr[i] = '\0';

    return attr;
}

void requestWOAnswer(SSL *ssl, char *namespace, char *data, char *destination_id)
{
    uint8_t buffer[4096];
    unsigned offset = 4;

    offset = pack_B(buffer, offset, getType(1, TYPE_ENUM));
    offset = pack_B(buffer, offset, PROTOCOL_VERSION);
    offset = pack_B(buffer, offset, getType(2, TYPE_STRING));
    offset = pack_B(buffer, offset, strlen(src));
    offset = pack_pct_ds(buffer, offset, src);
    offset = pack_B(buffer, offset, getType(3, TYPE_STRING));
    offset = pack_B(buffer, offset, strlen(destination_id));
    offset = pack_pct_ds(buffer, offset, destination_id);
    offset = pack_B(buffer, offset, getType(4, TYPE_STRING));
    offset = pack_B(buffer, offset, strlen(namespace));
    offset = pack_pct_ds(buffer, offset, namespace);
    offset = pack_B(buffer, offset, getType(5, TYPE_ENUM));
    offset = pack_B(buffer, offset, PAYLOAD_TYPE);
    offset = pack_B(buffer, offset, getType(6, TYPE_BYTES));
    offset = pack_pct_ds_glo(buffer, offset, getLenOf(data));
    offset = pack_pct_ds(buffer, offset, data);

    offset += pack_I(buffer, 0, offset);

    if((SSL_write(ssl, buffer, offset)) < 0)
    {
        fprintf(stderr, "Send error sur 1\n");
        exit(0);
    }
}

uint8_t *requestWAnswer(SSL *ssl, char *namespace, char *req, char *destination_id, int x, int *ind)
{
    uint8_t buffer[4096], *data;
    int i, n;
    unsigned offset = 4;

    /* Allocation de data */
    if(!(data = malloc(4096 * sizeof(uint8_t))))
    {
        fprintf(stderr, "Memoery error\n");
        exit(EXIT_FAILURE);
    }

    offset = pack_B(buffer, offset, getType(1, TYPE_ENUM));
    offset = pack_B(buffer, offset, PROTOCOL_VERSION);
    offset = pack_B(buffer, offset, getType(2, TYPE_STRING));
    offset = pack_B(buffer, offset, strlen(src));
    offset = pack_pct_ds(buffer, offset, src);
    offset = pack_B(buffer, offset, getType(3, TYPE_STRING));
    offset = pack_B(buffer, offset, strlen(destination_id));
    offset = pack_pct_ds(buffer, offset, destination_id);
    offset = pack_B(buffer, offset, getType(4, TYPE_STRING));
    offset = pack_B(buffer, offset, strlen(namespace));
    offset = pack_pct_ds(buffer, offset, namespace);
    offset = pack_B(buffer, offset, getType(5, TYPE_ENUM));
    offset = pack_B(buffer, offset, PAYLOAD_TYPE);
    offset = pack_B(buffer, offset, getType(6, TYPE_BYTES));
    offset = pack_pct_ds_glo(buffer, offset, getLenOf(req));
    offset = pack_pct_ds(buffer, offset, req);

    offset += pack_I(buffer, 0, offset);

    if((SSL_write(ssl, buffer, offset)) < 0)
    {
        fprintf(stderr, "Send error sur 2\n");
        exit(0);
    }

    *ind = 0;

    for(i = 0 ; i < x ; i++)
    {
        if((n = SSL_read(ssl, buffer, sizeof buffer)) >= 0)
        {
            for(i = 0 ; i < n ; i++)
                if(buffer[i])
                    data[(*ind)++] = buffer[i];
        }
        else
        {
            fprintf(stderr, "Erreur lors du read\n");
            exit(EXIT_FAILURE);
        }
    }

    return data;
}

