#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include "constants.h"
#include "chromecast.h"
#include "message.h"
#include "ssl.h"

extern Media MEDIA;

char *urn[] = {"urn:x-cast:com.google.cast.tp.connection", 
               "urn:x-cast:com.google.cast.receiver", 
               "urn:x-cast:com.google.cast.media"};

char *statusText[] = {"", "Ready To Cast", "Now Casting"};

/* Initialisation du chromecast */
Chromecast *initChromecast()
{
    int i;
    Chromecast *cast;

    /* Allocation des cast */
    if(!(cast = malloc(CAST_MAX * sizeof(Chromecast))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    for(i = 0 ; i < CAST_MAX ; i++)
    {
        strcpy(cast[i].ip, "");
        strcpy(cast[i].sender, "");
        cast[i].state = 0;
    }

    return cast;
}

/* Initialisation des données du chromecast */
void initReceiver(Chromecast *cast, int num, Protocol *dial)
{
    int i, j;
    char *data, state[255];

    VarSSL cssl;

    /* Allocation de data */
    if(!(data = malloc(4096 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    for(i = 0 ; i < num ; i++)
    {
        strcpy(cast[i].ip, dial[i].ip);

        cssl = initssl(cast[i].ip);

        connection(cssl.ssl, "receiver-0");

        data = getStatus(cssl.ssl);

        if(attrJSON(data, "\"statusText\""))
            strcpy(state, attrJSON(data, "\"statusText\""));
        else
            strcpy(state, "");

        for(j = 0 ; j < 3 ; j++)
            if(!(strcmp(state, statusText[j])))
                cast[i].state = j;

        SSL_free(cssl.ssl);
        close(cssl.server);
        X509_free(cssl.cert);
        SSL_CTX_free(cssl.ctx);
    }

    free(data);
}

/* Connexion au chromecast */
void connection(SSL *ssl, char *dest)
{
    requestWOAnswer(ssl, urn[0], "{\"type\":\"CONNECT\",\"origin\":{}}", dest); 
}

char *getStatus(SSL *ssl)
{
    char *data, *sessionId;
    int len = 0;
    char dest[255] = "receiver-0";

    /* Allocation de data */
    if(!(data = malloc(4096 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    /* Allocation de sessionId */
    if(!(sessionId = malloc(255 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    data = (char *) requestWAnswer(ssl, urn[1], "{\"type\":\"GET_STATUS\",\"requestId\":1}", dest, 1, &len);

    free(sessionId);

    return data;
}

char *getMediaStatus(SSL *ssl, char *dest)
{
    char *data;
    int len = 0;

    /* Allocation de data */
    if(!(data = malloc(4096 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    data = (char *) requestWAnswer(ssl, urn[2], "{\"type\":\"GET_STATUS\",\"requestId\":1}", dest, 1, &len);

    return data;
}

void *updateStatus(void *p)
{
    ThUpdateMedia *thum = (ThUpdateMedia *)p;
    Chromecast *cast = thum->cast;

    int i, j, num = thum->num;
    char *data, state[255];

    /* Allocation de data */
    if(!(data = malloc(4096 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    while(1)
    {
        for(i = 0 ; i < num ; i++)
        {
            VarSSL cssl = initssl(cast[i].ip);

            connection(cssl.ssl, "receiver-0");

            data = getStatus(cssl.ssl);

            if(attrJSON(data, "\"statusText\""))
                strcpy(state, attrJSON(data, "\"statusText\""));
            else
                strcpy(state, "");

            for(j = 0 ; j < 3 ; j++)
                if(!(strcmp(state, statusText[j])))
                    cast[i].state = j;

            SSL_free(cssl.ssl);
            close(cssl.server);
            X509_free(cssl.cert);
            SSL_CTX_free(cssl.ctx);
        }
    }

    free(data);

    return NULL;
}

void updateMedia(char *ip)
{
    VarSSL cssl = initssl(ip);

    char *data, dest[255];

    /* Allocation de data */
    if(!(data = malloc(4096 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    connection(cssl.ssl, "receiver-0");

    data = getStatus(cssl.ssl);
    strcpy(dest, attrJSON(data, "\"transportId\""));

    connection(cssl.ssl, dest);

    data = getMediaStatus(cssl.ssl, dest);

    if(attrJSON(data, "\"playerState\""))
        strcpy(MEDIA.playerState, attrJSON(data, "\"playerState\""));

    if(nbrJSON(data, "\"currentTime\""))
        MEDIA.currentTime = atof(nbrJSON(data, "\"currentTime\""));

    if(nbrJSON(data, "\"level\""))
        MEDIA.volumeLevel = atoi(nbrJSON(data, "\"level\""));

    if(nbrJSON(data, "\"muted\""))
        MEDIA.volumeMute = atoi(nbrJSON(data, "\"muted\""));

    if(nbrJSON(data, "\"width\""))
        MEDIA.width = atoi(nbrJSON(data, "\"width\""));

    if(nbrJSON(data, "\"height\""))
        MEDIA.height = atoi(nbrJSON(data, "\"height\""));

    if(attrJSON(data, "\"hdrType\""))
        strcpy(MEDIA.hdrType, attrJSON(data, "\"hdrType\""));

    if(nbrJSON(data, "\"duration\""))
        MEDIA.duration = atof(nbrJSON(data, "\"duration\""));

    SSL_free(cssl.ssl);
    close(cssl.server);
    X509_free(cssl.cert);
    SSL_CTX_free(cssl.ctx);

    free(data);
}

void printInfoMedia(Media *media)
{
    printf("PLAYER STATE : %s\n", media->playerState);
    printf("CURRENT TIME : %f\n", media->currentTime);
    printf("VOLUME LEVEL : %d\n", media->volumeLevel);
    printf("VOLUME MUTE : %d\n", media->volumeMute);
    printf("WIDTH : %d\n", media->width);
    printf("HEIGHT : %d\n", media->height);
    printf("HDR TYPE : %s\n", media->hdrType);
    printf("DURATION : %f\n", media->duration);
}

char *launchApp(char *ip)
{
    char *data, dest[255] = "receiver-0";
    int len;

    VarSSL ssl = initssl(ip);

    /* Allocation de data */
    if(!(data = malloc(4096 * sizeof(uint8_t))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    /* Connexion au chromecast */
    connection(ssl.ssl, dest);

    /* On récupère le status du chromecast */
    do {
        data = getStatus(ssl.ssl);
    } while(strstr(data, "\"PING\""));

    /* Chromecast non initiliser */
    if(strcmp("Ready To Cast", attrJSON(data, "\"statusText\"")))
        data = (char*)requestWAnswer(ssl.ssl, urn[1], "{\"type\":\"LAUNCH\",\"requestId\":2,\"appId\":\"CC1AD845\"}", "receiver-0", 2, &len);

    return data;
}

void load(SSL *ssl, char *session, char *dest, char *path)
{
    char *data = malloc(4096 * sizeof(char));
    char req[1024], mimetype[255];
    int len = 0;

    if(!strcmp(dest, "receiver-0"))
    {
        data = (char *) requestWAnswer(ssl, urn[1], "{\"type\":\"LAUNCH\",\"requestId\":2,\"appId\":\"CC1AD845\"}", "receiver-0", 2, &len);
        strcpy(dest, attrJSON(data, "\"transportId\""));
    }

    connection(ssl, dest);

    /* Détermine le type de format */
    if(strstr(path, "webm"))
        strcpy(mimetype, "video/webm");
    else
        strcpy(mimetype, "video/mp4");

    sprintf(req, "{\"type\":\"LOAD\",\"requestId\":3,\"sessionId\":\"%s\",\"media\":{\"contentId\":\"%s\",\"streamType\":\"live\",\"contentType\":\"%s\"},\"autoplay\":true,\"currentTime\":0,\"customData\":{\"payload\":{\"title:\":\"\"}}}", session, path, mimetype);

    requestWOAnswer(ssl, urn[2], req, dest);

    free(data);
}

void control(char *ip, char *type)
{
    char *data;
    char dest[255], req[1024], cmd[255];
    int mediaSessionId, len = 0;

    VarSSL ssl = initssl(ip);

    /* Allocation de data */
    if(!(data = malloc(4096 * sizeof(uint8_t))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    connection(ssl.ssl, "receiver-0");

    do {
        data = getStatus(ssl.ssl);
    } while(strstr(data, "\"PING\""));

    /* On récupère sessionId */
    if(attrJSON(data, "\"transportId\""))
        strcpy(dest, attrJSON(data, "\"transportId\""));
    else
    {
        fprintf(stderr, "Erreur lors de la connexion au chromecast\n");
        exit(EXIT_FAILURE);
    }

    connection(ssl.ssl, dest);

    data = getMediaStatus(ssl.ssl, dest);

    mediaSessionId = atoi(nbrJSON(data, "\"mediaSessionId\""));

    sprintf(req, "{\"type\":\"%s\",\"requestId\":1,\"mediaSessionId\":%d}", type, mediaSessionId);

    requestWAnswer(ssl.ssl, urn[2], req, dest, 1, &len);

    if(!(strcmp(type, "STOP")))
    {
        sprintf(cmd, "ps xawww | grep -i \"ffmpeg\" | awk '{print $1}' | while read pid ; do  kill $pid ; done > /dev/null 2>&1");
        system(cmd);
    }

    free(data);
}

void setPosition(char *ip, char *type, int sec)
{
    char *data;
    char dest[255], req[1024];
    int mediaSessionId, len = 0;

    VarSSL ssl = initssl(ip);

    /* Allocation de data */
    if(!(data = malloc(4096 * sizeof(uint8_t))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    connection(ssl.ssl, "receiver-0");

    do {
        data = getStatus(ssl.ssl);
    } while(strstr(data, "\"PING\""));

    /* On récupère sessionId */
    if(attrJSON(data, "\"transportId\""))
        strcpy(dest, attrJSON(data, "\"transportId\""));
    else
    {
        fprintf(stderr, "Erreur lors de la connexion au chromecast\n");
        exit(EXIT_FAILURE);
    }

    connection(ssl.ssl, dest);

    data = getMediaStatus(ssl.ssl, dest);

    mediaSessionId = atoi(nbrJSON(data, "\"mediaSessionId\""));

    sprintf(req, "{\"type\":\"%s\",\"requestId\":1,\"mediaSessionId\":%d,\"currentTime\":%d}", type, mediaSessionId, sec);

    requestWAnswer(ssl.ssl, urn[2], req, dest, 1, &len);

    free(data);
}

