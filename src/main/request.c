#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "request.h"
#include "constants.h"

int deleteRequest(char *hostname, int port, char *path)
{
    char buffer[1025] = { 0 };
    SOCKET sock;

    struct hostent *hostinfo = NULL;
    SOCKADDR_IN sin = { 0 };

    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sock == INVALID_SOCKET)
        return 1;

    if(!(hostinfo = gethostbyname(hostname)))
        return 1;

    sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
    sin.sin_port = htons(port);
    sin.sin_family = AF_INET;

    if(connect(sock,(SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
        return 1;

    sprintf(buffer, "DELETE %s HTTP/1.1\r\nHost: %s\r\nContent-Type: application/json\r\n\r\n", path, hostname);

    send(sock, buffer, sizeof buffer, 0);

    close(sock);

    return 0;
}

int postRequest(char *hostname, int port, char *path, char *params)
{
    char buffer[1025] = { 0 };
    SOCKET sock;

    struct hostent *hostinfo = NULL;
    SOCKADDR_IN sin = { 0 };

    int n;

    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sock == INVALID_SOCKET)
        return 1;

    if(!(hostinfo = gethostbyname(hostname)))
        return 1;

    sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
    sin.sin_port = htons(port);
    sin.sin_family = AF_INET;

    if(connect(sock,(SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
        return 1;

    if(params)
        sprintf(buffer, "POST %s HTTP/1.1\r\nHost: %s\r\nContent-Type: application/json\r\nContent-Length: %lu\r\n\r\n%s", path, hostname, strlen(params), params);
    else
        sprintf(buffer, "POST %s HTTP/1.1\r\nHost: %s\r\nContent-Type: application/json\r\n\r\n", path, hostname);

    send(sock, buffer, sizeof buffer, 0);
    n = recv(sock, buffer, sizeof buffer, 0);
    buffer[n] = '\0';

    close(sock);

    return 0;
}

