#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
 
#include "constants.h"
#include "server.h"

char *getHostname()
{
    FILE *f;
    struct hostent *he;
    char hostname[128];

    f = fopen("/proc/sys/kernel/hostname", "rt");
    fgets(hostname, 128, f);
    fclose(f);

    hostname[strlen(hostname) - 1] = '\0';

    if(!(he = gethostbyname(hostname)))
        return NULL;

    return he->h_name;
}

char *getIp()
{
    char *ip, *server = "8.8.8.8";
    int sock, port = 53;
    struct sockaddr_in sin, csin;
    socklen_t csinlen;
     
    if(!(ip = malloc(128 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    if(!(sock = socket (AF_INET, SOCK_DGRAM, 0)))
        return NULL;
     
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = inet_addr(server);
    sin.sin_port = htons(port);
 
    if(connect(sock, (const struct sockaddr*) &sin, sizeof(sin)))
        return NULL;
     
    csinlen = sizeof(csin);

    if(getsockname(sock, (struct sockaddr*) &csin, &csinlen))
        return NULL;
         
    inet_ntop(AF_INET, &csin.sin_addr, ip, 128);

    close(sock);
     
    return ip;
}

int sendResponse(SOCKET csock, const char *url)
{
    int n;
    char buffer[SIZE], curs[SIZE], mimetype[255];
    FILE *file;

    if(!(file = fopen(url, "rb")))
        return 1;

    /* Mime-Type */
    if(strstr(url, "webm"))
        strcpy(mimetype, "video/webm");
    else
        strcpy(mimetype, "video/mp4");

    /* Envoie de l'en-tête */
    sprintf(buffer, "HTTP/1.1 200\r\nContent-Type: %s\r\nAccess-Control-Allow-Origin: *\r\nTransfer-Encoding: chunked\r\n\r\n", mimetype);

    if((n = send(csock, buffer, strlen(buffer), 0)) < 0)
        return 2;

    /* Envoie du fichier */
    while((n = fread(curs, 1, sizeof curs, file)))
    {
        sprintf(buffer, "%.2X\r\n", n);
        if((send(csock, buffer, strlen(buffer), 0)) < 0)
            return 2;

        if((n = send(csock, curs, n, 0)) < 0)
            return 2;

        sprintf(buffer, "\r\n");
        if((n = send(csock, buffer, strlen(buffer), 0)) < 0)
            return 2;
    }

    /* Fin de l'envoie */
    strcpy(buffer, "0\r\n\r\n");
    if((n = send(csock, buffer, strlen(buffer), 0)) < 0)
        return 2;

    return 0;
}

int sendResponseStream(SOCKET csock)
{
    int n = 0;
    long size = 0, current = 0;
    char buffer[SIZE], curs[SIZE];
    FILE *file;

    /* Envoie de l'en-tête */
    sprintf(buffer, "HTTP/1.1 200\r\nContent-Type: video/webm\r\nAccess-Control-Allow-Origin: *\r\nTransfer-Encoding: chunked\r\n\r\n");

    if((n = send(csock, buffer, strlen(buffer), 0)) < 0)
        return 2;

    /* Envoie du fichier */
    while(1)
    {
        if(!(file = fopen("../videos/stream.webm", "rb")))
            return 1;

        /* Taille actuelle du fichier */
        fseek(file, 0, SEEK_END);
        size = ftell(file);

        if(current < size)
        {
            /* Lecture d'un bloc du fichier */
            fseek(file, current, SEEK_SET);
            if(!(n = fread(curs, 1, sizeof buffer, file)))
            {
                fclose(file);
                continue;
            }
            
            current+=n;

            sprintf(buffer, "%.2X\r\n", n);
            if((send(csock, buffer, strlen(buffer), 0)) < 0)
                return 2;

            if((n = send(csock, curs, n, 0)) < 0)
                return 2;

            sprintf(buffer, "\r\n");
            if((n = send(csock, buffer, strlen(buffer), 0)) < 0)
                return 2;
        }

        fclose(file);
    }

    return 0;
}

char *getUrl(const char *buffer)
{
    char *res, *url;
    int j, ind = 3;

    if(!(url = malloc(SIZE * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    res = strstr(buffer, "GET");

    /* Suppression des espaces */
    while(res[++ind] == ' ');

    if(res[ind] == '/' && res[++ind] == '?')
        for(j = ind+1 ; res[j] != ' ' ; j++)
            url[j - ind - 1] = res[j];

    url[j] = '\0';

    return url;
}

void *server(void *p)
{
    int n = 0, ret = -1, port = *((int *) p);
    char buffer[SIZE] = { 0 }, url[255];

    /* Socket du client */
    SOCKET csock;
    SOCKADDR_IN csin, sin;
    socklen_t recsize = sizeof(csin);

    /* Initialisation du serveur - Mise en place de la socket */
    SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
 
    /* Si la socket est invalide */
    if(sock == INVALID_SOCKET)
    {
        fprintf(stderr, "Impossible de créer la connexion.\n");
        exit(EXIT_FAILURE);
    }

    /* Configuration de la socket */
    sin.sin_addr.s_addr    = htonl(INADDR_ANY);   /* Adresse IP automatique */
    sin.sin_family         = AF_INET;             /* Protocole familial (IP) */
    sin.sin_port           = htons(port);        /* Listage du port */

    /* Connexion de la socket */
    if(bind(sock, (SOCKADDR*)&sin, sizeof(sin)) == SOCKET_ERROR)
    {
        fprintf(stderr, "La connexion du serveur sur le port %d a échoué.\n", port);
        exit(EXIT_FAILURE);
    }

    /* Ecoute de la socket sur le port PORT */
    if(listen(sock, 5) == SOCKET_ERROR)
    {
        fprintf(stderr, "Erreur de connexion.\n");
        exit(EXIT_FAILURE);
    }

    while(1)
    {   
        csock = accept(sock, (SOCKADDR*)&csin, &recsize);

        /* Recoit les donnees du client (couple pseudo/password) */
        if((n = recv(csock, buffer, sizeof buffer, 0)) < 0)
        {
            fprintf(stderr, "Erreur lors de la réception de données\n");
            exit(EXIT_FAILURE);
        }

        buffer[n] = '\0';

        /* Explode du GET - Récupération de l'URL du fichier */

        if(port < 20000)
        {
            strcpy(url, getUrl(buffer));
            ret = sendResponse(csock, url);
        }
        else if(port < 40000)
            ret = sendResponseStream(csock);

        switch(ret)
        {
            case -1:
                fprintf(stderr, "Le port %d est invalide\n", port);
                pthread_exit(NULL);
                break;
            case 1:
                fprintf(stderr, "Ressource non trouvée\n");
                pthread_exit(NULL);
                break;
            case 2:
                close(sock);
                pthread_exit(NULL);
                break;
            default:
                break;
        }
    }

    /* Fermeture de la socket */
    close(sock);
 
    pthread_exit(NULL);
}

