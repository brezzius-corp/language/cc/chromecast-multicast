#ifndef EVENTS_H_INCLUDED
#define EVENTS_H_INCLUDED

typedef struct
{
    char key[SDLK_LAST]; /* SDLK_LAST */
    int mousex,mousey;
    int mousexrel,mouseyrel;
    char mousebuttons[8];
    char quit;
} Input;

int keypress(Input *in);
void UpdateEvents(Input *in);

#endif /* EVENTS_H_INCLUDED */
