#ifndef __CHROMECAST_H
#define __CHROMECAST_H

#include "constants.h"
#include "media.h"

typedef struct {
    char ip[39];
    int state;
    char sender[255];
} Chromecast;

typedef struct {
    Protocol *dial;
    Chromecast *cast;
    int num;
} ThUpdateMedia;

Chromecast *initChromecast();
void initReceiver(Chromecast *cast, int num, Protocol *dial);
void connection(SSL *ssl, char *dest);
char *getMediaStatus(SSL *ssl, char *dest);
char *getStatus(SSL *ssl);
void *updateStatus(void *p);
void updateMedia(char *ip);
char *launchApp(char *ip);
void load(SSL *ssl, char *session, char *dest, char *path);
void control(char *ip, char *type);
void setPosition(char *ip, char *type, int sec);

#endif /* __CHROMECAST_H */

