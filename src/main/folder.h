#ifndef __FOLDER_H
#define __FOLDER_H

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#include "constants.h"
#include "events.h"

#define SPEED_WHEEL 30

#define BASE_LIST_DIR 40

#define NBR_FILE_MAX 11
#define TIME_POPUP 1500

#define WIDTH_SCROLL 20

#define COLOR_FOLDER_R MENU_COLOR_R
#define COLOR_FOLDER_G MENU_COLOR_G
#define COLOR_FOLDER_B MENU_COLOR_B

#define COLOR_OFOLDER_R 34
#define COLOR_OFOLDER_G 41
#define COLOR_OFOLDER_B 47

typedef struct dirent DIRENT;
typedef struct stat STAT;

typedef struct {
    SDL_Surface *surf;
    SDL_Rect rect;
    char text[256];
    int width;
    int height;
} Rep;

char *directory(SDL_Surface *screen, Input *in, TTF_Font **font, char *cwpath);
Rep *listdir(TTF_Font **font, char *path, int *len, int *error);
void errorAccess(SDL_Surface *fold, TTF_Font **font, char *text);

#endif /* __FOLDER_H */
