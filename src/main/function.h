#ifndef __FUNCTION_H
#define __FUNCTION_H

#include "constants.h"
#include "gfx.h"

extern int NUM_DEVICES;

int isint(const char *str);
void setDial(Protocol *dial);
char *nodeXML(const char *haystack, const char *needle);
void *discoverDevices(void *p);
void msleep(int tms);

#endif /* __FUNCTION_H */
