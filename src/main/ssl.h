#ifndef __SSL_H
#define __SSL_H

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509_vfy.h>

typedef struct {
    SSL *ssl;
    int server;
    X509 *cert;
    SSL_CTX *ctx;
} VarSSL;

VarSSL initssl(char *dest_url);
int create_socket(char *hostname);

#endif /* __SSL_H */

