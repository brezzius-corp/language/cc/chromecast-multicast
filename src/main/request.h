#ifndef __REQUEST_H
#define __REQUEST_H

int deleteRequest(char *hostname, int port, char *path);
int postRequest(char *hostname, int port, char *path, char *params);

#endif /* __REQUEST_H */
