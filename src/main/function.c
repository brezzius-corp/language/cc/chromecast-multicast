#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include "constants.h"
#include "function.h"
#include "ssl.h"

int isint(const char *str)
{
    int size = strlen(str), i;

    if(size <= 0)
        return -1;

    for(i = 0 ; i < size ; i++)
    {
        if(str[i] < 48 || str[i] > 57)
            return -1;
    }

    return 0;
}

void setDial(Protocol *dial)
{
    int i, j, len = strlen(dial->url), size, bytes = 0;
    char tmp[255], csize[255], *search, *flux;
    char buffer[SIZE+1] = { 0 }, data[2*SIZE+1] = { 0 };
    int n = 0;
    SOCKET sock;
    SOCKADDR_IN sin = { 0 };

    /* IP du device */
    for(i = 7 ; dial->url[i] != ':' ; i++)
        dial->ip[i-7] = dial->url[i];

    dial->ip[(i++)-7] = '\0';

    /* Port du device */
    for(j = i ; dial->url[j] != '/' ; j++)
        tmp[j-i] = dial->url[j];

    tmp[j-i] = '\0';

    if(!isint(tmp))
        dial->port = atoi(tmp);

    /* Path du device */
    for(i = j ; i < len ; i++)
        dial->path[i-j] = dial->url[i];

    dial->path[i-j] = '\0';

    /* Nom du device */
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sock == INVALID_SOCKET)
    {
        fprintf(stderr, "Erreur lors de la connexion.\n");
        exit(EXIT_FAILURE);
    }

    sin.sin_addr = *(IN_ADDR *) gethostbyname(dial->ip)->h_addr;
    sin.sin_family = AF_INET;
    sin.sin_port = htons(dial->port);

    if(connect(sock,(SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
    {
        fprintf(stderr, "Impossible de se connecter au serveur.\nVeuillez vérifier votre connexion internet.\n");
        exit(EXIT_FAILURE);
    }

    sprintf(buffer, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", dial->path, dial->ip);
    send(sock, buffer, strlen(buffer), 0);

    do
    {
        n = recv(sock, buffer, sizeof(buffer) - 1 , 0);
        buffer[n] = '\0';

        if(n > 0)
            strcat(data, buffer);

        bytes+=n;
    } while(n == sizeof buffer - 1);

    /* Fermeture de la connexion */
    close(sock);

    /* Longueur du fichier XML */
    if((search = (strstr(data, "Content-Length:") + 15)))
    {
        for(i = 0 ;; i++)
            if(search[i] == '\r' && search[i+1] == '\n')
            {
                csize[i] = '\0';
                break;
            }
            else
                csize[i] = search[i];

        if(!isint(csize))
            size = atoi(csize);
    }

    /* Allocation de flux */
    if(!(flux = malloc(size * sizeof(char) + 1)))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    /* Formatage des données */
    for(i = 0 ; i < bytes-4 ; i++)
        if(data[i] == '\r' && data[i+1] == '\n' && data[i+2] == '\r' && data[i+3] == '\n')
            break;

    i+=4;

    for(j = 0 ; j < size-1 ; j++)
        flux[j] = data[i+j];

    strcpy(dial->name, nodeXML(flux, "friendlyName"));
    strcpy(dial->manufacturer, nodeXML(flux, "manufacturer"));
    strcpy(dial->model, nodeXML(flux, "modelName"));
    strcpy(dial->uuid, nodeXML(flux, "UDN"));
    strcpy(dial->icon.url, nodeXML(flux, "url"));
    strcpy(dial->icon.mimetype, nodeXML(flux, "mimetype"));
    dial->icon.width = atoi(nodeXML(flux, "width"));
    dial->icon.height = atoi(nodeXML(flux, "height"));
    dial->icon.depth = atoi(nodeXML(flux, "depth"));
}

char *nodeXML(const char *haystack, const char *needle)
{
    char *fnode, *lnode, *init, *last, *text;
    int i = 0, len = strlen(needle), size;

    /* Allocation de fnode et lnode */
    if(!(fnode = malloc((len + 3) * sizeof(char))) || !(lnode = malloc((len + 4) * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    fnode[0] = '<';
    lnode[0] = '<';
    lnode[1] = '/';

    for(i = 0 ; i < len ; i++)
    {
        fnode[i+1] = needle[i];
        lnode[i+2] = needle[i];
    }

    fnode[i+1] = '>';
    lnode[i+2] = '>';
    fnode[i+2] = '\0';
    lnode[i+3] = '\0';

    /* Allocation de text */
    if(!(text = malloc(255 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    if((init = (strstr(haystack, fnode) + len+2)))
    {
        if(!(last = (strstr(haystack, lnode))))
        {
            fprintf(stderr, "Une erreur est survenu lors de la récupération des données\n");
            exit(EXIT_FAILURE);
        }

        size = last - init;

        for(i = 0 ; i < size ; i++)
            text[i] = init[i];

        text[i] = '\0';
    }
    else
        return "Unknow";

    return text;
}

void *discoverDevices(void *p)
{
    char buffer[SIZE + 1] = { 0 }, *data;
    int i, n, rank = -1;
    SOCKET sock;
    SOCKADDR_IN csin = { 0 }, sin = { 0 };
    socklen_t length = sizeof(sin);

    Protocol *dial = (Protocol *) p;

    /* Timeout socket */
    struct timeval tv, ta, tb;
    tv.tv_sec = DELAY_SEND_PACKET;
    tv.tv_usec = 0;

    /* Création de la socket */
    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(sock == INVALID_SOCKET)
    {
        fprintf(stderr, "Erreur lors de la connexion.\n");
        exit(EXIT_FAILURE);
    }

    csin.sin_family = AF_INET;
    csin.sin_addr.s_addr = htonl(INADDR_ANY);
    csin.sin_port = htons(0);

    if(bind(sock,(SOCKADDR *) &csin, sizeof(csin)) == SOCKET_ERROR)
    {
        fprintf(stderr, "Impossible de se connecter au serveur.\nVeuillez vérifier votre connexion internet.\n");
        exit(EXIT_FAILURE);
    }

    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = inet_addr("239.255.255.250");
    sin.sin_port = htons(1900);

    /* Paramètre socket */
    setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char*) &tv, sizeof(struct timeval));

    /* Requête M-SEARCH */
    sprintf(buffer, "M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: \"ssdp:discover\"\r\nMX: 1\r\nST: urn:dial-multiscreen-org:service:dial:1\r\n\r\n");
    sendto(sock, buffer, strlen(buffer), 0, (SOCKADDR *) &sin, sizeof(sin));

    do
    {
        rank++;

        gettimeofday(&ta, NULL);

        if((n = recvfrom(sock, buffer, sizeof(buffer) - 1, 0, (SOCKADDR *) &sin, &length)) < 0)
            break;

        gettimeofday(&tb, NULL);

        /* Nouveau delay de la socket déterminé à partir du temps de réponse précédent */
        tv.tv_sec = ((tb.tv_sec*1e6 + tb.tv_usec) - (ta.tv_sec*1e6 + ta.tv_usec)) / 1e6 + 2;
        setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char*) &tv, sizeof(struct timeval));

        buffer[n] = '\0';

        if(!(data = strstr(buffer, "LOCATION: ")))
            return 0;

        for(i = 10 ; data[i] != '\r' && data[i+1] != '\n'  ; i++)
            dial[rank].url[i-10] = data[i];

        dial[rank].url[i-10] = '\0';

        setDial(&(dial[rank]));
    } while(n > 0);

    close(sock);

    NUM_DEVICES = rank;

    return NULL;
}

void msleep(int tms)
{
    struct timeval tv;
    tv.tv_sec  = tms / 1000;
    tv.tv_usec = (tms % 1000) * 1000;
    select(0, NULL, NULL, NULL, &tv);
}

