#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <resolv.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509_vfy.h>

#include "constants.h"
#include "ssl.h"

/* ---------------------------------------------------------- *
 * First we need to make a standard TCP socket connection.    *
 * create_socket() creates a socket & TCP-connects to server. *
 * ---------------------------------------------------------- */

VarSSL initssl(char *dest_url) {

    BIO               *outbio = NULL;
    const SSL_METHOD *method;

    VarSSL ssl;

    /* ---------------------------------------------------------- *
    * These function calls initialize openssl for correct work.  *
    * ---------------------------------------------------------- */
    OpenSSL_add_all_algorithms();
    ERR_load_BIO_strings();
    ERR_load_crypto_strings();
    SSL_load_error_strings();

    /* ---------------------------------------------------------- *
    * Create the Input/Output BIO's.                             *
    * ---------------------------------------------------------- */
    outbio  = BIO_new_fp(stdout, BIO_NOCLOSE);

    /* ---------------------------------------------------------- *
    * initialize SSL library and register algorithms             *
    * ---------------------------------------------------------- */
    if(SSL_library_init() < 0)
        BIO_printf(outbio, "Could not initialize the OpenSSL library !\n");

    /* ---------------------------------------------------------- *
    * Set SSLv2 client hello, also announce SSLv3 and TLSv1      *
    * ---------------------------------------------------------- */
    method = SSLv23_client_method();

    /* ---------------------------------------------------------- *
    * Try to create a new SSL context                            *
    * ---------------------------------------------------------- */
    if ( (ssl.ctx = SSL_CTX_new(method)) == NULL)
        BIO_printf(outbio, "Unable to create a new SSL context structure.\n");

    /* ---------------------------------------------------------- *
    * Disabling SSLv2 will leave v3 and TSLv1 for negotiation    *
    * ---------------------------------------------------------- */
    SSL_CTX_set_options(ssl.ctx, SSL_OP_NO_SSLv2);

    /* ---------------------------------------------------------- *
    * Create new SSL connection state object                     *
    * ---------------------------------------------------------- */
    ssl.ssl = SSL_new(ssl.ctx);

    /* ---------------------------------------------------------- *
    * Make the underlying TCP socket connection                  *
    * ---------------------------------------------------------- */
    ssl.server = create_socket(dest_url);

    /* ---------------------------------------------------------- *
    * Attach the SSL session to the socket descriptor            *
    * ---------------------------------------------------------- */
    SSL_set_fd(ssl.ssl, ssl.server);

    /* ---------------------------------------------------------- *
    * Try to SSL-connect here, returns 1 for success             *
    * ---------------------------------------------------------- */
    if ( SSL_connect(ssl.ssl) != 1 )
        BIO_printf(outbio, "Error: Could not build a SSL session to: %s.\n", dest_url);

    /* ---------------------------------------------------------- *
    * Get the remote certificate into the X509 structure         *
    * ---------------------------------------------------------- */
    ssl.cert = SSL_get_peer_certificate(ssl.ssl);
    if (ssl.cert == NULL)
        BIO_printf(outbio, "Error: Could not get a certificate from: %s.\n", dest_url);

    return ssl;
}

/* ---------------------------------------------------------- *
 * create_socket() creates the socket & TCP-connect to server *
 * ---------------------------------------------------------- */
int create_socket(char *hostname)
{
    int port = 8009;
    SOCKET sock;
    struct hostent *host;
    SOCKADDR_IN sin;

    if((host = gethostbyname(hostname)) == NULL )
    {
        fprintf(stderr, "Cannot resolve hostname %s.\n",  hostname);
        exit(EXIT_FAILURE);
    }

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
    {
        fprintf(stderr, "Erreur lors de la connexion\n");
        exit(EXIT_FAILURE);
    }

    sin.sin_addr = *(IN_ADDR *) gethostbyname(hostname)->h_addr;
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);

    if(connect(sock, (SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
    {
        fprintf(stderr, "Impossible de se connecter au serveur. Veuillez vérifier votre connexion internet\n");
        exit(EXIT_FAILURE);
    }

    return sock;
}

