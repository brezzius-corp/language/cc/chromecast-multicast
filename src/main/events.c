#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>

#include "events.h"

int keypress(Input *in)
{
    int i;

    for(i = 0 ; i < SDLK_LAST ; i++)
        if(in->key[i])
            return 1;

    return 0;
}

void UpdateEvents(Input *in)
{
    SDL_Event event;
	
    in->mousebuttons[SDL_BUTTON_WHEELUP] = 0;
    in->mousebuttons[SDL_BUTTON_WHEELDOWN] = 0;

    while(SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_KEYDOWN:
                in->key[event.key.keysym.sym] = 1;
                break;
            case SDL_KEYUP:
                in->key[event.key.keysym.sym] = 0;
                break;
            case SDL_MOUSEMOTION:
                in->mousex=event.motion.x;
                in->mousey=event.motion.y;
                in->mousexrel=event.motion.xrel;
                in->mouseyrel=event.motion.yrel;
                break;
            case SDL_MOUSEBUTTONDOWN:
                in->mousebuttons[event.button.button] = 1;
                break;
            case SDL_MOUSEBUTTONUP:
                if((event.button.button != SDL_BUTTON_WHEELUP) && (event.button.button != SDL_BUTTON_WHEELDOWN))
                    in->mousebuttons[event.button.button] = 0;
                break;
            case SDL_QUIT:
                in->quit = 1;
                break;
            default:
                break;
        }
    }
}
