#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

#include "constants.h"
#include "folder.h"
#include "events.h"
#include "input.h"

void errorAccess(SDL_Surface *fold, TTF_Font **font, char *text)
{
    int width, height;
    SDL_Surface *surface, *stext;
    SDL_Rect psurface, ptext;
    SDL_Color red = {255, 0, 0, 255};

    surface = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_POPUP, HEIGHT_POPUP, 32, 0, 0, 0, 0);
    SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, 102, 102, 102));

    psurface.x = WIDTH_DIR / 2 - WIDTH_POPUP / 2;
    psurface.y = HEIGHT_DIR / 2 - HEIGHT_POPUP / 2;

    TTF_SizeText(font[SIZE_FONT_D], text, &width, &height);
    stext = TTF_RenderUTF8_Blended(font[SIZE_FONT_D], text, red);

    ptext.x = WIDTH_POPUP / 2 - width / 2;
    ptext.y = HEIGHT_POPUP / 2 - height / 2;

    SDL_BlitSurface(stext, NULL, surface, &ptext);
    SDL_BlitSurface(surface, NULL, fold, &psurface);
    SDL_FreeSurface(stext);
    SDL_FreeSurface(surface);
}

Rep *listdir(TTF_Font **font, char *path, int *len, int *error)
{
    int i = 0;
    char newpath[255];
    DIR *dir;
    Rep *rep;
    DIRENT *current;
    STAT *st;
    SDL_Color blue = {0, 0, 255, 255}, white = {233, 233, 233, 255};

    /* Ouverture du répertoire */
    if(!(dir = opendir(path)))
    {
        switch(errno)
        {
            case EACCES:
                *error = 2;
                return NULL;
            default:
                fprintf(stderr, "Impossible d'accéder au dossier courant : %s\n", strerror(errno));
                *error = 1;
                return NULL;
        }
    }
    else
        *error = 0;

    *len = 1;

    /* Premier parcours pour compter le nombre de fichier et de dossier */
    while((current = readdir(dir)))
        if(current->d_name[0] != '.')
            (*len)++;

    /* Allocation de rep */
    if(!(rep = malloc(*len * sizeof(Rep))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    rewinddir(dir);

    st = malloc(sizeof(STAT));

    /* Enregistrement du dossier parent */
    strcpy(rep[i].text, "..");
    TTF_SizeText(font[SIZE_FONT_D], rep[0].text, &(rep[0].width), &(rep[0].height));
    rep[0].surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_D], rep[i].text, blue);
    rep[0].rect.x = 5;
    rep[0].rect.y = BASE_LIST_DIR;

    /* Second parcours */
    while((current = readdir(dir)))
    {
        if(current->d_name[0] != '.')
        {
            i++;
            strcpy(newpath, path);
            strcat(newpath, current->d_name);

            sprintf(rep[i].text, "%s", current->d_name);
            TTF_SizeText(font[SIZE_FONT_D], rep[i].text, &(rep[i].width), &(rep[i].height));

            if(stat(newpath, st) == 0)
            {
                if(S_ISDIR(st->st_mode))
                    rep[i].surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_D], rep[i].text, blue);
                else
                    rep[i].surf = TTF_RenderUTF8_Blended(font[SIZE_FONT_D], rep[i].text, white);
            }

            rep[i].rect.x = 5;
            rep[i].rect.y = rep[i-1].rect.y + rep[i-1].height + 10;
        }
    }

    closedir(dir);

    *error = 0;
    return rep;
}

char *directory(SDL_Surface *screen, Input *in, TTF_Font **font, char *cwpath)
{
    int len = 0, pos = 0, max = 0, error = 0, ta = 0, tb, taerr[2] = { 0 }, tberr, i, j;
    int ccyscroll, icyscroll, scrollactive = 0, pathactive = 0, pchar = 0;
    char *path, home[255], rscPath[255];
    SDL_Surface *fold, *close, *outline[4], *title, *stitle, *ctitle, *scroll, *scrollbar;
    SDL_Rect pfold, pclose, poutline[4], ptitle = {5, 1, 0, 0}, sptitle = {5, 5, 0, 0}, cptitle = {1, 1, 0, 0}, pscroll, pscrollbar = {0, 0, 0, 0};
    SDL_Color white = {233, 233, 233, 255}, bg = {COLOR_FOLDER_R, COLOR_FOLDER_G, COLOR_FOLDER_B, 255};
    Rep *rep, *tmp;
    STAT *st = malloc(sizeof(STAT));

    strcpy(home, getenv("HOME"));
    strcat(home, "/");

    fold = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_DIR, HEIGHT_DIR, 32, 0, 0, 0, 0);
    SDL_FillRect(fold, NULL, SDL_MapRGB(screen->format, 0, 0, 0));

    outline[0] = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_DIR, 1, 32, 0, 0, 0, 0);
    outline[1] = SDL_CreateRGBSurface(SDL_HWSURFACE, 1, HEIGHT_DIR, 32, 0, 0, 0, 0);
    outline[2] = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_DIR, 1, 32, 0, 0, 0, 0);
    outline[3] = SDL_CreateRGBSurface(SDL_HWSURFACE, 1, HEIGHT_DIR, 32, 0, 0, 0, 0);

    poutline[0].x = 0;
    poutline[0].y = 0;
    poutline[1].x = WIDTH_DIR - 1;
    poutline[1].y = 0;
    poutline[2].x = 0;
    poutline[2].y = HEIGHT_DIR - 1;
    poutline[3].x = 0;
    poutline[3].y = 0;

    /* Chargement de l'icone Close */
    sprintf(rscPath, "%s/../src/main/resources/image/close_popup.png", cwpath);
    if(!(close = IMG_Load(rscPath)))
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    /* Création de la surface contenaire du titre */
    stitle = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_DIR - close->w - 30, 30, 32, 0, 0, 0, 0);
    SDL_FillRect(stitle, NULL, SDL_MapRGB(stitle->format, 233, 233, 233));
    ctitle = SDL_CreateRGBSurface(SDL_HWSURFACE, stitle->w - 2, stitle->h - 2, 32, 0, 0, 0, 0);

    pfold.x = WIDTH_WINDOW / 2 - WIDTH_DIR / 2;
    pfold.y = HEIGHT_WINDOW / 2 - HEIGHT_DIR / 2;
    pclose.x = WIDTH_DIR - close->w - 10;
    pclose.y = 10;

    /* Création de la surface scroll */
    scroll = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_SCROLL, HEIGHT_DIR - close->w - 30, 32, 0, 0, 0, 0);
    pscroll.x = WIDTH_DIR - WIDTH_SCROLL - 10;
    pscroll.y = pclose.y + close->w + 10;

    rep = listdir(font, home, &len, &error);

    /* Allocation de path */
    if(!(path = malloc(255 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    strcpy(path, home);
    pchar = strlen(path);

    while(!in->quit)
    {
        /* Gestion des évènements */
        UpdateEvents(in);

        /* Nettoyage de l'écran */
        SDL_FillRect(fold, NULL, SDL_MapRGB(fold->format, COLOR_FOLDER_R, COLOR_FOLDER_G, COLOR_FOLDER_B));

        /* Nettoyage de la surface du path */
        SDL_FillRect(ctitle, NULL, SDL_MapRGB(stitle->format, COLOR_FOLDER_R, COLOR_FOLDER_G, COLOR_FOLDER_B));

        /* Création de la surface scrollbar */
        if(len > NBR_FILE_MAX)
            scrollbar = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_SCROLL, scroll->h * (100*NBR_FILE_MAX/len)/100, 32, 0, 0, 0, 0);
        else
            scrollbar = SDL_CreateRGBSurface(SDL_HWSURFACE, WIDTH_SCROLL, scroll->h, 32, 0, 0, 0, 0);

        SDL_FillRect(scrollbar, NULL, SDL_MapRGB(scrollbar->format, 100, 100, 100));

        /* Clique sur la zone de path */
        if(pathactive || (in->mousex > sptitle.x + pfold.x && in->mousex < sptitle.x + pfold.x + stitle->w &&
           in->mousey > sptitle.y + pfold.y && in->mousey < sptitle.y + pfold.y + stitle->h && in->mousebuttons[SDL_BUTTON_LEFT]))
       {
            if(in->key[SDLK_ESCAPE])
                pathactive = 0;
            else
                pathactive = 1;

            if(input(ctitle, in, font[SIZE_FONT_D], bg, white, path, 255, &pchar, 0))
            {
                in->key[SDLK_RETURN] = 0;
                pathactive = 0;
                if(strlen(path) > 1 && path[strlen(path) - 1] == '/')
                {
                    path[strlen(path) - 1] = '\0';
                    pchar--;
                }

                if(stat(path, st) == 0)
                {
                    if(S_ISDIR(st->st_mode))
                    {
                        if(strlen(path) > 1)
                        {
                            path[strlen(path)] = '/';
                            pchar++;
                        }

                        if((tmp = listdir(font, path, &len, &error)))
                            rep = tmp;
                        else
                        {
                            /* On supprime le répertoire sélectionner du path */
                            path[strlen(path) - 1] = 0;
                            for(i = strlen(path) - 2 ; path[i] != '/' ; i--)
                                path[i] = 0;

                            taerr[0] = SDL_GetTicks();
                            pchar = strlen(path);
                        }
                    }
                    else
                        return path;
                }
                else
                {
                    /* On supprime le répertoire sélectionner du path */
                    path[strlen(path) - 1] = 0;
                    for(i = strlen(path) - 2 ; path[i] != '/' ; i--)
                        path[i] = 0;

                    taerr[1] = SDL_GetTicks();
                }
            }
        }

        /* Appuie sur la touche q - Quitte le programme */
        if(in->key[SDLK_q])
        {
            in->key[SDLK_q] = 0;
            return NULL;
        }

        /* Appuie sur la croix - Ferme la popup */
        if(in->mousex > pclose.x + pfold.x && in->mousex < pclose.x + pfold.x + close->w &&
           in->mousey > pclose.y + pfold.y && in->mousey < pclose.y + pfold.y + close->h && in->mousebuttons[SDL_BUTTON_LEFT])
        {
            in->mousebuttons[SDL_BUTTON_LEFT] = 0;
            return NULL;
        }

        /* Molette de la souris - Bas */
        if(in->mousebuttons[SDL_BUTTON_WHEELDOWN])
        {
            max = (HEIGHT_DIR - BASE_LIST_DIR) / (rep[1].height + 10) - 1;
            if(pos < len - max)
            {
                /* Move file */
                pos+=1;
                for(i = 0 ; i < len ; i++)
                    rep[i].rect.y -= SPEED_WHEEL;

                /* Move scrollbar */
                pscrollbar.y += (scroll->h - scrollbar->h) / (len - NBR_FILE_MAX);
            }
        }

        /* Molette de la souris - Haut */
        if(in->mousebuttons[SDL_BUTTON_WHEELUP])
        {
            if(pos > 0)
            {
                /* Move file */
                pos-=1;
                for(i = 0 ; i < len ; i++)
                    rep[i].rect.y += SPEED_WHEEL;

                /* Move scrollbar */
                pscrollbar.y -= (scroll->h - scrollbar->h) / (len - NBR_FILE_MAX);
            }
        }

        /* Clique sur la scrollbar */
        if(in->mousex > pscroll.x + pfold.x && in->mousex < pscroll.x + scroll->w + pfold.x && in->mousebuttons[SDL_BUTTON_LEFT] &&
           in->mousey > pscroll.y + pscrollbar.y + pfold.y && in->mousey < pscroll.y + pscrollbar.y + scrollbar->h + pfold.y && len > NBR_FILE_MAX)
        {
            if(!scrollactive)
            {
                scrollactive = 1;
                icyscroll = in->mousey;
            }
            else
            {
                ccyscroll = in->mousey;

                /* Move scrollbar - Bas */
                if(ccyscroll - icyscroll > (scroll->h - scrollbar->h) / (len - NBR_FILE_MAX))
                {
                    max = (HEIGHT_DIR - BASE_LIST_DIR) / (rep[1].height + 10) - 1;
                    if(pos < len - max)
                    {
                        /* Move file */
                        pos+=1;
                        for(i = 0 ; i < len ; i++)
                            rep[i].rect.y -= SPEED_WHEEL;

                        /* Move scrollbar */
                        pscrollbar.y += (scroll->h - scrollbar->h) / (len - NBR_FILE_MAX);
                    }
                }
                else if(icyscroll - ccyscroll > (scroll->h - scrollbar->h) / (len - NBR_FILE_MAX))
                {
                    if(pos > 0)
                    {
                        /* Move file */
                        pos-=1;
                        for(i = 0 ; i < len ; i++)
                        rep[i].rect.y += SPEED_WHEEL;

                        /* Move scrollbar */
                        pscrollbar.y -= (scroll->h - scrollbar->h) / (len - NBR_FILE_MAX);
                    }
                }
            }
        }
        else if(in->mousebuttons[SDL_BUTTON_LEFT] && scrollactive)
            scrollactive = 0;

        /* Selection d'un dossier ou d'un fichier */
        for(i = 0 ; i < len ; i++)
        {
            if(in->mousex > rep[i].rect.x + pfold.x && in->mousex < rep[i].rect.x + rep[i].width + pfold.x &&
               in->mousey > rep[i].rect.y + pfold.y && in->mousey < rep[i].rect.y + rep[i].height + pfold.y && in->mousebuttons[SDL_BUTTON_LEFT])
            {
                in->mousebuttons[SDL_BUTTON_LEFT] = 0;

                /* Réinitialisation des indexes de parcours */
                pos = 0;
                pscrollbar.y = 0;

                /* Retour dans le dossier parent */
                if(!strcmp(rep[i].text, ".."))
                {
                    /* On ne peut plus revenir en arrière à la racine */
                    if(strlen(path) > 1)
                    {
                        path[strlen(path) - 1] = 0;
                        for(j = strlen(path) - 2 ; path[j] != '/' ; j--)
                            path[j] = 0;
                    }
                }
                else
                {
                    strcat(path, rep[i].text);
                    strcat(path, "/");
                }

                if(strlen(path) > 1)
                    path[strlen(path) - 1] = '\0';

                if(stat(path, st) == 0)
                {
                    if(S_ISDIR(st->st_mode))
                    {
                        if(strlen(path) > 1)
                            path[strlen(path)] = '/';

                        if((tmp = listdir(font, path, &len, &error)))
                            rep = tmp;
                        else
                        {
                            /* On supprime le répertoire sélectionner du path */
                            path[strlen(path) - 1] = 0;
                            for(i = strlen(path) - 2 ; path[i] != '/' ; i--)
                                path[i] = 0;

                            taerr[0] = SDL_GetTicks();
                        }
                    }
                    else
                        return path;
                }
                pchar = strlen(path);
            }
        }

        /* Clic en-dehors de la popup */
        if((((in->mousex < pfold.x) || (in->mousex > pfold.x + fold->w) || (in->mousey < pfold.y) || (in->mousey > pfold.y + fold->h)) && (in->mousebuttons[SDL_BUTTON_LEFT])))
        {
            in->mousebuttons[SDL_BUTTON_LEFT] = 0;
            for(i = 0 ; i < 4 ; i++)
                SDL_FillRect(outline[i], NULL, SDL_MapRGB(outline[i]->format, 255, 0, 0));

            ta = SDL_GetTicks();
        }
        else
        {
            tb = SDL_GetTicks();

            if((tb - ta) > 500)
                for(i = 0 ; i < 4 ; i++)
                    SDL_FillRect(outline[i], NULL, SDL_MapRGB(outline[i]->format, COLOR_OFOLDER_R, COLOR_OFOLDER_G, COLOR_OFOLDER_B));
        }

        for(i = 0 ; i < len ; i++)
        {
            if(rep[i].rect.x > 0 && rep[i].rect.x < WIDTH_DIR - rep[i].width && rep[i].rect.y > BASE_LIST_DIR - 10 && rep[i].rect.y < HEIGHT_DIR - rep[i].height)
                SDL_BlitSurface(rep[i].surf, NULL, fold, &(rep[i].rect));
        }

        tberr = SDL_GetTicks();

        if(taerr[0] && (tberr - taerr[0] < TIME_POPUP))
            errorAccess(fold, font, "Acces refuse");
        else if(taerr[1] && (tberr - taerr[1] < TIME_POPUP))
            errorAccess(fold, font, "Dossier inexistant");

        /* Blit du répertoire courant */
        if(!pathactive)
        {
            title = TTF_RenderUTF8_Blended(font[SIZE_FONT_D], path, white);
            SDL_BlitSurface(title, NULL, ctitle, &ptitle);
        }
        SDL_BlitSurface(ctitle, NULL, stitle, &cptitle);

        SDL_FillRect(scroll, NULL, SDL_MapRGB(scroll->format, 200, 200, 200));
        SDL_BlitSurface(scrollbar, NULL, scroll, &pscrollbar);
        SDL_BlitSurface(scroll, NULL, fold, &pscroll);
        SDL_FreeSurface(scrollbar);

        SDL_BlitSurface(stitle, NULL, fold, &sptitle);

        for(i = 0 ; i < 4 ; i++)
            SDL_BlitSurface(outline[i], NULL, fold, &poutline[i]);

        SDL_BlitSurface(close, NULL, fold, &pclose);

        SDL_BlitSurface(fold, NULL, screen, &pfold);

        SDL_Flip(screen);
    }

    for(i = 0 ; i < 4 ; i++)
        SDL_FreeSurface(outline[i]);

    SDL_FreeSurface(rep->surf);
    SDL_FreeSurface(fold);

    return NULL;
}

