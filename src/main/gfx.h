#ifndef __GFX_H
#define __GFX_H

#include <SDL/SDL_ttf.h>

#include "chromecast.h"

#define COLOR_BUTTON 237

typedef struct {
    SDL_Surface *surf;
    SDL_Rect rect;
} Image;

typedef struct {
    SDL_Surface *surf;
    SDL_Rect rect;
    int width;
    int height;
    char text[255];
} Surface;

typedef struct {
    SDL_Surface *button;
    SDL_Surface *surf;
    SDL_Rect rect;
    SDL_Rect psurf;
    SDL_Rect grad;
    int width;
    int height;
    char text[255];
} Button;

typedef struct {
    Image img;
    Surface name, manufacturer, model, ip, state;
    Button reboot;
    Button launch;
    Button video;
    Button stream;
    Button webcam;
    Button stop;
    Button reset;
} GFX_Chromecast;

typedef struct {
    Image img;
    Surface name, ip;
    Button reboot;
    Button launch;
    Button video;
    Button stream;
    Button webcam;
    Button stop;
    Button reset;
} GFX_Client;

typedef struct {
    SDL_Surface **surface;
    SDL_Rect rect;
    int current;
} GIF_Image;

GFX_Chromecast *GFX_InitChromecast(char *path);
GIF_Image GIF_Init(int len, char *path, SDL_Rect rect, char *cwpath);
void GFX_InitReceiver(Chromecast *castinfo, GFX_Chromecast *cast, int num, Protocol *dial, TTF_Font **font, char *path);
void GFX_InitClient(GFX_Client *client, TTF_Font **font);
void GFX_FreeChromecast(GFX_Chromecast *cast);
void GFX_UpdateReceiver(Chromecast *castinfo, GFX_Chromecast *cast, int num, TTF_Font **font, char *path);

#endif /* __GFX_H */
