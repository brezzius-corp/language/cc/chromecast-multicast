#ifndef __INPUT_H
#define __INPUT_H

    #include <SDL/SDL.h>
    #include <SDL/SDL_ttf.h>
	#include "events.h"

    int input(SDL_Surface *surface, Input *in, TTF_Font *font, SDL_Color bg, SDL_Color color, char *string, int max, int *pchar, int shown);
	void blitCursor(SDL_Rect *ptext, TTF_Font *font, char *chainEntry, SDL_Surface *surface, int *positionCaractere);
	void readChar(Input *in, char *chaine, int *positionCaractere, int max);
	int detecterLettre(Input *in, int *letterEntry, char *caractereEntre, int *capital, int *flags);
	void entrerLettre(int *letterEntry, long numeroCaractere, char *caractere);
	void insererCaractereDansChaine(int *positionCaractere, char *chaine, char caractereAInserer);
 
#endif /* __INPUT_H */

